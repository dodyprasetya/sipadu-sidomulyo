package com.sipadu.android.util

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.media.RingtoneManager
import android.os.Build
import androidx.core.app.NotificationCompat
import androidx.core.content.ContextCompat
import com.sipadu.android.R
import kotlin.random.Random

object NotificationUtil {

    const val NOTIFICATION_REQUEST_CODE = 0;

    fun showNotification(
        context: Context, intent: PendingIntent, title: String, message: String, priority: String
    ) {
        val channelId = context.resources.getString(R.string.push_notification_channel_id)
        val channelName = context.resources.getString(R.string.push_notification_channel_name)
        val defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)

        val notificationBuilder = NotificationCompat.Builder(context, channelId)
            .setSmallIcon(R.drawable.icon_notification)
            .setColor(ContextCompat.getColor(context, R.color.primary))
            .setColorized(true)
            .setContentTitle(title)
            .setContentText(message)
            .setAutoCancel(true)
            .setSound(defaultSoundUri)
            .setContentIntent(intent)

        if (priority == "high") {
            notificationBuilder
                .setPriority(NotificationCompat.PRIORITY_HIGH)
                .setVibrate(longArrayOf(1000, 1000, 1000))
        }

        val notificationManager = context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

        // Since android Oreo notification channel is needed.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val channel = NotificationChannel(channelId, channelName, NotificationManager.IMPORTANCE_DEFAULT)
            notificationManager.createNotificationChannel(channel)
        }

        val r = Random
        val randomNumber = r.nextInt(100 - 1) + 1
        val notificationId = (System.currentTimeMillis() / 1000).toInt() + randomNumber;
        notificationManager.notify(notificationId, notificationBuilder.build())
    }
}