package com.sipadu.android.util

import android.Manifest
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Build
import android.provider.Settings
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.karumi.dexter.Dexter
import com.karumi.dexter.MultiplePermissionsReport
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.multi.MultiplePermissionsListener
import es.dmoral.toasty.Toasty
import java.util.*

object PermissionUtil {

    fun askStorageAndCameraPermissions(activity: AppCompatActivity) {
        Dexter.withActivity(activity)
            .withPermissions(
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.READ_EXTERNAL_STORAGE,
                Manifest.permission.CAMERA,
            )
            .withListener(object : MultiplePermissionsListener {
                override fun onPermissionsChecked(report: MultiplePermissionsReport) {
                    handlePermissionResult(report, activity)
                }

                override fun onPermissionRationaleShouldBeShown(
                    permissions: MutableList<PermissionRequest>?,
                    token: PermissionToken?
                ) {
                    token!!.continuePermissionRequest()
                }

            }).check()
    }

    fun askLocationPermissions(activity: AppCompatActivity) {
        Dexter.withActivity(activity)
            .withPermissions(
                Manifest.permission.ACCESS_COARSE_LOCATION,
                Manifest.permission.ACCESS_FINE_LOCATION
            )
            .withListener(object : MultiplePermissionsListener {
                override fun onPermissionsChecked(report: MultiplePermissionsReport) {
                    handlePermissionResult(report, activity)
                }

                override fun onPermissionRationaleShouldBeShown(
                    permissions: MutableList<PermissionRequest>?,
                    token: PermissionToken?
                ) {
                    token!!.continuePermissionRequest()
                }

            }).check()
    }

    @RequiresApi(Build.VERSION_CODES.Q)
    fun askBackgroundLocationPermissions(activity: AppCompatActivity) {
        Dexter.withActivity(activity)
            .withPermissions(
                Manifest.permission.ACCESS_COARSE_LOCATION,
                Manifest.permission.ACCESS_FINE_LOCATION,
                Manifest.permission.ACCESS_BACKGROUND_LOCATION
            )
            .withListener(object : MultiplePermissionsListener {
                override fun onPermissionsChecked(report: MultiplePermissionsReport) {
                    handlePermissionResult(report, activity)
                }

                override fun onPermissionRationaleShouldBeShown(
                    permissions: MutableList<PermissionRequest>?,
                    token: PermissionToken?
                ) {
                    token!!.continuePermissionRequest()
                }

            }).check()
    }

    fun isLocationPermissionGranted(context: Context) = (
        ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION)
                == PackageManager.PERMISSION_GRANTED &&
        ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED
    )

    @RequiresApi(Build.VERSION_CODES.Q)
    fun isBackgroundLocationPermissionGranted(context: Context) = (
        ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION)
            == PackageManager.PERMISSION_GRANTED &&
        ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION)
            == PackageManager.PERMISSION_GRANTED &&
        ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_BACKGROUND_LOCATION)
            == PackageManager.PERMISSION_GRANTED
    )

    private fun handlePermissionResult(
        report: MultiplePermissionsReport, activity: AppCompatActivity
    ) {
        if (report.isAnyPermissionPermanentlyDenied) {
            Toasty.info(activity,
                "Anda sudah menolak izin secara permanen, silahkan berikan izin melalui settings aplikasi",
                Toasty.LENGTH_LONG).show()

            Timer().schedule(object : TimerTask() {
                override fun run() {
                    ContextCompat.startActivity(activity, Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS)
                        .apply { data = Uri.fromParts("package", activity.packageName, null) }, null)

                    activity.finish()
                }
            }, 1500)
        }
        else if (!report.areAllPermissionsGranted()) {
            Toasty.info(activity,
                "Anda sudah menolak izin, silahkan berikan izin untuk melanjutkan",
                Toasty.LENGTH_LONG).show()

            Timer().schedule(object : TimerTask() {
                override fun run() { activity.finish() }
            }, 1000)
        }
    }
}