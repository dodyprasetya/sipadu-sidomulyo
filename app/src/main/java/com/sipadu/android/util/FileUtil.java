package com.sipadu.android.util;

import android.content.Context;
import android.os.Environment;
import android.webkit.MimeTypeMap;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * Created by dody on 11/17/17.
 */

public class FileUtil {

    private static FileUtil instance = new FileUtil();
    private final String APP_FOLDER = "/Sipadu";
    private final String APP_FOLDER_TEMP = "/Sipadu/temp";
    private final String APP_CACHE_FILE = "/Sipadu/cache";
    private final String APP_AUDIO_FILE = "/Sipadu/audio";
    private final String APP_DOWNLOAD_FILE = "/Sipadu/download";

    private final String TEMP_PREFIX = "temp";
    private final String TEMP_SUFFIX = ".tmp";

    public static String TEMP_FOLDER_PATH;
    public static String APP_FOLDER_PATH;
    public static String CACHE_FOLDER_PATH;
    public static String AUDIO_FOLDER_PATH;
    public static String DOWNLOAD_FOLDER_PATH;

    private FileUtil() {
        APP_FOLDER_PATH = Environment.getExternalStorageDirectory().toString() + APP_FOLDER;
        TEMP_FOLDER_PATH = Environment.getExternalStorageDirectory().toString() + APP_FOLDER_TEMP;
        CACHE_FOLDER_PATH = Environment.getExternalStorageDirectory().toString() + APP_CACHE_FILE;
        AUDIO_FOLDER_PATH = Environment.getExternalStorageDirectory().toString() + APP_AUDIO_FILE;
        DOWNLOAD_FOLDER_PATH = Environment.getExternalStorageDirectory().toString() + APP_DOWNLOAD_FILE;

        File storageDir = new File(APP_FOLDER_PATH);
        if (!storageDir.exists()) {
            storageDir.mkdirs();
        }
        storageDir = new File(TEMP_FOLDER_PATH);
        if (!storageDir.exists()) {
            storageDir.mkdirs();
        }
        storageDir = new File(CACHE_FOLDER_PATH);
        if (!storageDir.exists()) {
            storageDir.mkdirs();
        }
        storageDir = new File(AUDIO_FOLDER_PATH);
        if (!storageDir.exists()) {
            storageDir.mkdirs();
        }
        storageDir = new File(DOWNLOAD_FOLDER_PATH);
        if (!storageDir.exists()) {
            storageDir.mkdirs();
        }
    }

    public static FileUtil getInstance() {
        return instance;
    }

    public File createTempFile(Context context) {
        return createTempFile(context, TEMP_PREFIX, TEMP_SUFFIX);
    }

    public File createTempFile(Context context, String prefix, String suffix) {
        File storageDir = new File(TEMP_FOLDER_PATH);

        if (!storageDir.exists()) {
            storageDir.mkdirs();
        }

        File file = null;
        try {
            file = File.createTempFile(prefix, suffix, storageDir);
        } catch (IOException e) {
            e.printStackTrace();
            File detro = new File(context.getCacheDir(), TEMP_FOLDER_PATH);
            if (!detro.exists()) {
                detro.mkdirs();
            }
            try {
                file = File.createTempFile(prefix, suffix, detro);
            } catch (IOException e1) {
                e1.printStackTrace();
            }
        }
        return file;
    }

    public void deleteTempFiles() {
        File dir = new File(TEMP_FOLDER_PATH);
        if (dir.isDirectory()) {
            if (dir.list() != null) {
                String[] children = dir.list();
                for (int i = 0; i < children.length; i++) {
                    File file = new File(dir, children[i]);
                    String path = file.getAbsolutePath();
                    String filename = path.substring(path.lastIndexOf("/") + 1);
                    if (filename.startsWith(TEMP_PREFIX)) {
                        file.delete();
                    }
                }
            }
        }
    }

    public void copy(File source, File destination) throws IOException {
        InputStream in = new FileInputStream(source);
        try {
            OutputStream out = new FileOutputStream(destination);
            try {
                // Transfer bytes from in to out
                byte[] buf = new byte[1024];
                int len;
                while ((len = in.read(buf)) > 0) {
                    out.write(buf, 0, len);
                }
            } finally {
                out.close();
            }
        } finally {
            in.close();
        }
    }

    public String getMimeType(String url) {
        String type = null;
        String extension = MimeTypeMap.getFileExtensionFromUrl(url);
        if (extension != null) {
            type = MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension);
        }
        return type;
    }

    public File prepareDocumentFile(File selectedFile, String selectedFilePath) throws IOException {

        // rename file
        String newPath = FileUtil.TEMP_FOLDER_PATH + "/"
                + selectedFile.getName()
                .replaceAll("[;\\/:*?\"<>|&']", "")
                .replaceAll(" ", "_");

        File newFile;

        if (!selectedFilePath.equals(newPath)) {

            newFile = new File(newPath);

            // if in temp exist, delete it
            if (newFile.exists()) newFile.delete();

            // copy file
            copy(selectedFile, newFile);

        } else {
            newFile = new File(selectedFilePath);
        }

        return newFile;
    }
}
