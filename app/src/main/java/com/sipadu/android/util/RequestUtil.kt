package com.sipadu.android.util

import android.net.Uri
import okhttp3.MediaType
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody
import okhttp3.RequestBody.Companion.asRequestBody
import okhttp3.RequestBody.Companion.toRequestBody
import java.io.File

object RequestUtil {

    fun getBody(param: String): RequestBody {
//        return RequestBody.create(MediaType.parse("text/plain"), param)
//        return RequestBody.create("text/plain".toMediaTypeOrNull(), param)
        return param.toRequestBody("text/plain".toMediaTypeOrNull())
    }

    fun getFileBody(fileUri: Uri): MultipartBody.Part {
        val filePath = fileUri.path!!
        val mimeType = FileUtil.getInstance().getMimeType(filePath)
        val file = File(filePath)

//        val requestFile = RequestBody.create(MediaType.parse(mimeType), file)
        val requestFile = file.asRequestBody(mimeType.toMediaTypeOrNull())
        val fileRequestBody = MultipartBody.Part.createFormData(
            "file", file.getName(), requestFile)

        return fileRequestBody
    }
}