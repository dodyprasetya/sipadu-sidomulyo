//package com.sipadu.android.util;
//
//import android.app.NotificationChannel;
//import android.app.NotificationManager;
//import android.app.PendingIntent;
//import android.content.Context;
//import android.content.Intent;
//import android.media.RingtoneManager;
//import android.net.Uri;
//import android.os.Build;
//import androidx.core.app.NotificationCompat;
//import androidx.core.content.ContextCompat;
//
//import com.bursadesain.android.R;
//import com.bursadesain.android.app.Constants;
//import com.bursadesain.android.event.NotificationEvent;
//import com.bursadesain.android.model.Notification;
//import com.bursadesain.android.model.Profile;
//import com.bursadesain.android.ui.chat.ChatMessageActivity;
//import com.bursadesain.android.ui.login.LoginActivity;
//import com.bursadesain.android.ui.order.OrderDetailActivity;
//
//import org.greenrobot.eventbus.EventBus;
//
//import java.util.ArrayList;
//import java.util.Calendar;
//import java.util.Map;
//import java.util.Random;
//
//public class NotificationUtil {
//
//    private static final int NOTIFICATION_REQUEST_CODE = 0;
//
//    private static final String MESSAGE = "message";
//    private static final String TYPE = "type";
//    private static final String ORDER_ID = "order_id";
//    private static final String USER_ID = "user_id";
//    private static final String ROOM_ID = "room_id";
//    private static final String ROOM_NAME = "room_name";
//    private static final String ROOM_PIC = "room_pic";
//
//    private static final String TYPE_ORDER = "ORDER";
//    private static final String TYPE_CHAT = "CHAT";
//
//    private static NotificationUtil instance;
//
//    public static NotificationUtil getInstance() {
//        if (instance == null) {
//            instance = new NotificationUtil();
//        }
//        return instance;
//    }
//
//    public void showNotification(Context context, Map<String, String> data, String from) {
//
//        if (AppUtil.INSTANCE.isAppIsInBackground(context)){
//
//            // notification if app in background
//            showNotificationTray(context, data, from, true);
//
//        } else {
//
//            // notify homepage to update new notification circle
////            EventBus.getDefault().post(new NotificationEvent(NotificationEvent.NEW_NOTIFICATION));
//
//            // notification if app in foreground
//            showNotificationTray(context, data, from, false);
//        }
//    }
//
//    public void showNotificationTray(Context context, Map<String, String> data, String from, boolean isBackground) {
//
//        Intent intent;
//
//        if (from.equals("/topics/" + Constants.FIREBASE_GLOBAL_TOPIC)) {
//
//            // update csv push notif
//            intent = new Intent(context, LoginActivity.class);
//
//        } else {
//            // push notif with type
//
//            if (data.get(TYPE) == null) return;
//
//            intent = new Intent(context, LoginActivity.class);
//
//            if (data.get(TYPE).equals(TYPE_ORDER)) {
//
//                String orderId = data.get(ORDER_ID);
//                intent = new Intent(context, OrderDetailActivity.class);
//                intent.putExtra("order_id", orderId);
//
//                Notification obj = new Notification();
//                obj.setType(TYPE_ORDER);
//                obj.setMessage(data.get(MESSAGE));
//                obj.setId(orderId);
//
//                saveNotification(context, obj);
//            }
//            else if (data.get(TYPE).equals(TYPE_CHAT)) {
//
//                String userId = data.get(USER_ID);
//                Profile profile = AppUtil.INSTANCE.getProfile(context);
//                if (userId.equals(profile.getId())) return;
//
//                String roomId = data.get(ROOM_ID);
//                String roomName = data.get(ROOM_NAME);
//                String roomPic = data.get(ROOM_PIC);
//
//                intent = new Intent(context, ChatMessageActivity.class);
//                intent.putExtra("room_id", roomId);
//                intent.putExtra("room_name", roomName);
//                intent.putExtra("room_pic", roomPic);
//
//                Notification obj = new Notification();
//                obj.setType(TYPE_CHAT);
//                obj.setMessage(data.get(MESSAGE));
//                obj.setId(roomId);
//                obj.setRoom_name(roomName);
//                obj.setRoom_pic(roomPic);
//
//                saveNotification(context, obj);
//            }
//        }
//
//        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
//
//        // if foreground
////        if (!isBackground) {
////            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
////        }
//
//        PendingIntent pendingIntent = PendingIntent.getActivity(context,
//                NOTIFICATION_REQUEST_CODE, intent, PendingIntent.FLAG_UPDATE_CURRENT);
//
//        showSmallNotification(context, pendingIntent, data.get(MESSAGE));
//
//    }
//
//    public void showSmallNotification(Context context, PendingIntent pendingIntent, String message){
//
//        String channelId = context.getResources().getString(R.string.push_notification_channel_id);
//        String channelName = context.getResources().getString(R.string.push_notification_channel_name);
//        String channelDescription = context.getResources().getString(R.string.push_notification_channel_description);
//
//        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
//        NotificationCompat.Builder notificationBuilder =
//                new NotificationCompat.Builder(context, channelId)
//                        .setSound(defaultSoundUri)
//                        .setSmallIcon(getNotificationIcon())
//                        .setContentTitle(context.getResources().getString(R.string.app_name))
//                        .setContentText(message)
//                        .setColor(ContextCompat.getColor(context, R.color.colorPrimary))
//                        .setAutoCancel(true)
//                        .setContentIntent(pendingIntent);
//
//        NotificationManager notificationManager =
//                (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
//
//        if (Build.VERPendingIntentSION.SDK_INT >= Build.VERSION_CODES.O) {
//            if (notificationManager == null) return;
//
//            int importance = NotificationManager.IMPORTANCE_HIGH;
//            NotificationChannel mChannel = notificationManager.getNotificationChannel(channelId);
//            if (mChannel == null) {
//                mChannel = new NotificationChannel(channelId, channelName, importance);
//                mChannel.setDescription(channelDescription);
//                notificationManager.createNotificationChannel(mChannel);
//            }
//        }
//
//        Random r = new Random();
//        int random_numb = r.nextInt(100 - 1) + 1;
//        int notificationId = ((int) (System.currentTimeMillis() / 1000)) + random_numb;
//        notificationManager.notify(notificationId, notificationBuilder.build());
//    }
//
//    private int getNotificationIcon() {
//        boolean useWhiteIcon = (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP);
//        if (useWhiteIcon)
//            return R.mipmap.ic_launcher;
//        else
//            return R.mipmap.ic_launcher;
//    }
//
//    private void saveNotification(Context context, Notification obj) {
//        TinyDB tinyDB = new TinyDB(context);
//
//        Calendar cal = Calendar.getInstance();
//        obj.setDate(DateUtil.INSTANCE.getFormattedCalendar(cal, DateUtil.LOCAL_DATE_FORMAT));
//
//        int newNotif = tinyDB.getInt("new_notification_count");
//        ArrayList<Object> list = tinyDB.getListObject("notifications", Notification.class);
//        list.add(obj);
//
//        tinyDB.putListObject("notifications", list);
//        tinyDB.putInt("new_notification_count", newNotif + 1);
//
////        EventBus.getDefault().post(new NotificationEvent(NotificationEvent.TYPE_NEW_NOTIF));
//    }
//}
