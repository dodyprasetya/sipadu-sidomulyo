package com.sipadu.android.util

import android.app.AlarmManager
import android.app.PendingIntent
import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Build
import android.os.SystemClock
import androidx.appcompat.app.AppCompatActivity
import com.sipadu.android.app.Constants
import com.sipadu.android.background.LocationBootReceiver
import com.sipadu.android.background.LocationReceiver

object AlarmUtil {

    const val LOCATION_ALARM_REQ_CODE = 1000

    fun startLocationAlarm(context: Context) {
        val alarmManager = context.getSystemService(AppCompatActivity.ALARM_SERVICE) as AlarmManager
        val alarmReceiver = Intent(context, LocationReceiver::class.java)
        val pendingIntent = PendingIntent.getBroadcast(context, LOCATION_ALARM_REQ_CODE, alarmReceiver,
            PendingIntent.FLAG_UPDATE_CURRENT)

        val interval = Constants.LOCATION_UPDATE_INTERVAL

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            alarmManager.setAndAllowWhileIdle(
                AlarmManager.ELAPSED_REALTIME,
                SystemClock.elapsedRealtime() + interval, pendingIntent)
        } else {
            alarmManager.setInexactRepeating(
                AlarmManager.ELAPSED_REALTIME,
                SystemClock.elapsedRealtime() + interval, interval, pendingIntent)
        }

        enableLocationBootReceiver(context)
    }

    fun cancelLocationAlarm(context: Context) {
        val alarmManager = context.getSystemService(AppCompatActivity.ALARM_SERVICE) as AlarmManager
        val alarmReceiver = Intent(context, LocationReceiver::class.java)
        val pendingIntent = PendingIntent.getBroadcast(context, LOCATION_ALARM_REQ_CODE, alarmReceiver,
            PendingIntent.FLAG_NO_CREATE or PendingIntent.FLAG_CANCEL_CURRENT)

        if (pendingIntent != null) {
            alarmManager.cancel(pendingIntent)
            disableLocationBootReceiver(context)
        }
    }

    fun enableLocationBootReceiver(context: Context) {
        val receiver = ComponentName(context, LocationBootReceiver::class.java)
        context.packageManager.setComponentEnabledSetting(
            receiver,
            PackageManager.COMPONENT_ENABLED_STATE_ENABLED,
            PackageManager.DONT_KILL_APP
        )
    }

    fun disableLocationBootReceiver(context: Context) {
        val receiver = ComponentName(context, LocationBootReceiver::class.java)
        context.packageManager.setComponentEnabledSetting(
            receiver,
            PackageManager.COMPONENT_ENABLED_STATE_DISABLED,
            PackageManager.DONT_KILL_APP
        )
    }
}