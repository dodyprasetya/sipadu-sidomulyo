package com.sipadu.android.app

object Constants {
    const val FIREBASE_GLOBAL_TOPIC = "globalPush"
    const val FIREBASE_INFO_TOPIC = "info"

    const val PREF_PROFILE = "pref_profile"
    const val PREF_FCM_TOKEN = "pref_fcm_token"
    const val PREF_SHOULD_UPDATE_FCM_TOKEN = "pref_should_update_fcm_token"
    const val PREF_IS_LOCATION_TRACKER_RUNNING = "pref_is_location_tracker_running"
    const val PREF_LAST_UPDATE_LOCATION = "pref_last_update_location"

    const val LOCATION_UPDATE_INTERVAL = 10 * 60000L // 10 minute interval

    const val ADMIN_WHATSAPP_NUMBER = "081259564185"
}