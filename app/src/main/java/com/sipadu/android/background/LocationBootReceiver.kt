package com.sipadu.android.background

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import com.sipadu.android.util.AlarmUtil

class LocationBootReceiver : BroadcastReceiver() {

    override fun onReceive(context: Context, intent: Intent?) {
        intent?.let {
            if (it.action == "android.intent.action.BOOT_COMPLETED") {
                // restart location request
                AlarmUtil.startLocationAlarm(context)
            }
        }
    }
}