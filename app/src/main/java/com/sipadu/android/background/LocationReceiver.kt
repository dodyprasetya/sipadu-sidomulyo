package com.sipadu.android.background

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.os.Build
import com.google.android.gms.location.*
import com.sipadu.android.app.Constants
import com.sipadu.android.server.ServerManager
import com.sipadu.android.util.AlarmUtil
import com.sipadu.android.util.AppUtil
import com.sipadu.android.util.PermissionUtil
import com.sipadu.android.util.PrefUtil
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import java.util.concurrent.TimeUnit

class LocationReceiver : BroadcastReceiver() {

    private lateinit var fusedLocationClient: FusedLocationProviderClient

    private val compositeDisposable = CompositeDisposable()

    override fun onReceive(context: Context, intent: Intent?) {
        // checking location permission
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            if (!PermissionUtil.isBackgroundLocationPermissionGranted(context)) return
        } else {
            if (!PermissionUtil.isLocationPermissionGranted(context)) return
        }

        fusedLocationClient = LocationServices.getFusedLocationProviderClient(context)

        getCurrentLocation(context)
        restartAlarm(context)
    }

    private fun restartAlarm(context: Context) {
        // restart alarm only on SDK >= 23 because of doze mode
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            AlarmUtil.startLocationAlarm(context)
        }
    }

    private fun getCurrentLocation(context: Context) {
        fusedLocationClient.lastLocation
            .addOnSuccessListener { location ->
                // getting the last known or current location
                location?.let {
                    val latitude = location.latitude
                    val longitude = location.longitude

                    updateStaffLocation(context, latitude.toString(), longitude.toString())
                }
            }
            .addOnFailureListener {}
    }

    private fun updateStaffLocation(context: Context, lat: String, lng: String) {
        if (AppUtil.isLogin(context)) {
            AppUtil.getProfile(context)?.let {
                if (it.type.equals("staff")) {
                    val disposable = ServerManager.getInstance()
                        .service.trackLocation(it.id, lat, lng)
                        .subscribeOn(Schedulers.io())
                        .observeOn(Schedulers.io())
                        .debounce(400, TimeUnit.MILLISECONDS)
                        .subscribe(
                            {
                                val time = (System.currentTimeMillis() / 1000).toString()
                                PrefUtil.write(context, Constants.PREF_LAST_UPDATE_LOCATION, time)
                            }, {}
                        )

                    compositeDisposable.add(disposable)
                }
            }
        }
    }
}