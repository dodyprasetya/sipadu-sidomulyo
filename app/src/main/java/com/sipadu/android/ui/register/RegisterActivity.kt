package com.sipadu.android.ui.register

import android.app.ProgressDialog
import android.content.Intent
import android.graphics.Bitmap
import android.net.Uri
import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.sipadu.android.R
import com.sipadu.android.app.Constants
import com.sipadu.android.server.ServerManager
import com.sipadu.android.util.PermissionUtil
import com.sipadu.android.util.PrefUtil
import com.sipadu.android.util.RequestUtil
import com.yalantis.ucrop.UCrop
import es.dmoral.toasty.Toasty
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_register.*
import pl.aprilapps.easyphotopicker.EasyImage
import pl.aprilapps.easyphotopicker.MediaFile
import pl.aprilapps.easyphotopicker.MediaSource
import java.io.File
import java.util.concurrent.TimeUnit

class RegisterActivity : AppCompatActivity() {

    lateinit var dialog: ProgressDialog
    private var imageUri: Uri? = null

    private val compositeDisposable = CompositeDisposable()

    lateinit var easyImage: EasyImage;

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)

        initProgressDialog()

        easyImage = EasyImage.Builder(this).build()

        backIcon.setOnClickListener {
            onBackPressed()
        }

        btSignUp.setOnClickListener {
            signUp()
        }
    }

    override fun onStart() {
        super.onStart()
        PermissionUtil.askStorageAndCameraPermissions(this)
    }

    private fun initProgressDialog() {
        dialog = ProgressDialog(this);
        dialog.setMessage("Mohon tunggu..");
        dialog.setIndeterminate(true);
        dialog.setCanceledOnTouchOutside(false);
        dialog.setCancelable(false);
    }

    private fun signUp() {
        val fullname = etFullname.text.toString()
        val phone = etPhone.text.toString()
        val nik = etNik.text.toString()
        val kk = etKK.text.toString()
        val rt = etRT.text.toString()
        val rw = etRW.text.toString()
        val password = etPassword.text.toString()
        val confirmPassword = etConfirmPassword.text.toString()

        if (
            !TextUtils.isEmpty(fullname) &&
            !TextUtils.isEmpty(kk) &&
            !TextUtils.isEmpty(rt) &&
            !TextUtils.isEmpty(rw) &&
            !TextUtils.isEmpty(nik) &&
            !TextUtils.isEmpty(phone) &&
            !TextUtils.isEmpty(password) &&
            !TextUtils.isEmpty(confirmPassword)
        ) {
            if (password != confirmPassword) {
                Toasty.info(this, "Password tidak sama", Toasty.LENGTH_LONG).show()
            } else {
                postUser()
            }
        } else {
            Toasty.info(this, "Mohon isi semua data yang bertanda *", Toasty.LENGTH_LONG).show()
        }
    }

    private fun postUser() {
        val fcmToken = PrefUtil.read(this, Constants.PREF_FCM_TOKEN, "") ?: ""

        val fullname = etFullname.text.toString()
        val email = etEmail.text.toString()
        val address = etAddress.text.toString()
        val phone = etPhone.text.toString()
        val nik = etNik.text.toString()
        val password = etPassword.text.toString()
        val kk = etKK.text.toString()
        val rt = etRT.text.toString().toInt().toString() // to strip 0 on the front
        val rw = etRW.text.toString().toInt().toString() // to strip 0 on the front

        val user = hashMapOf(
            "email" to RequestUtil.getBody(email),
            "fullname" to RequestUtil.getBody(fullname),
            "password" to RequestUtil.getBody(password),
            "address" to RequestUtil.getBody(address),
            "nik" to RequestUtil.getBody(nik),
            "phone" to RequestUtil.getBody(phone),
            "no_kk" to RequestUtil.getBody(kk),
            "rt" to RequestUtil.getBody(rt),
            "rw" to RequestUtil.getBody(rw),
            "active" to RequestUtil.getBody("0"),
            "position" to RequestUtil.getBody("Warga"),
            "type" to RequestUtil.getBody("user"),
            "fcm_token" to RequestUtil.getBody(fcmToken)
        )

        val observable: Observable<String>

        if (imageUri != null) {
            val fileRequestBody = RequestUtil.getFileBody(imageUri!!)
            observable = ServerManager.getInstance().service.signUp(user, fileRequestBody)
        } else {
            observable = ServerManager.getInstance().service.signUpWithoutImage(user)
        }

        val disposable = observable
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .debounce(400, TimeUnit.MILLISECONDS)
            .doOnSubscribe { dialog.show() }
            .doOnTerminate { dialog.dismiss() }
            .subscribe(
                {
                    if (it.equals("Berhasil")) {
                        Toasty.success(
                            this,
                            "Pendaftaran berhasil, mohon tunggu konfirmasi Admin untuk mengaktifkan akun Anda",
                            Toasty.LENGTH_LONG
                        ).show()
                        finish()
                    } else if (it.equals("Gagal Upload") || it.equals("No Image")) {
                        Toasty.error(this, "Upload gambar gagal, silahkan coba lagi", Toasty.LENGTH_LONG).show()
                    } else if (it.equals("Terdaftar")) {
                        Toasty.error(this, "Akun sudah terdaftar, silahkan login.", Toasty.LENGTH_LONG).show()
                    } else {
                        Toasty.error(this, "Pendaftaran gagal, silahkan coba lagi", Toasty.LENGTH_LONG).show()
                    }
                },
                {
                    Toasty.error(this, "Pendaftaran gagal, silahkan coba lagi", Toasty.LENGTH_LONG).show()
                }
            )

        compositeDisposable.add(disposable)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        easyImage.handleActivityResult(requestCode, resultCode, data, this, object : EasyImage.Callbacks {
            override fun onImagePickerError(error: Throwable, source: MediaSource) {
                Toasty.error(this@RegisterActivity, "Error dalam mengambil gambar", Toasty.LENGTH_LONG).show()
                error.printStackTrace()
            }

            override fun onCanceled(source: MediaSource) {
            }

            override fun onMediaFilesPicked(imageFiles: Array<MediaFile>, source: MediaSource) {
                if (imageFiles.size > 0) {

                    imageUri = Uri.fromFile(imageFiles[0].file)

                    val options = UCrop.Options()
                    options.setCompressionFormat(Bitmap.CompressFormat.JPEG)
                    options.setCompressionQuality(80)
                    options.setShowCropGrid(true)

                    UCrop.of(imageUri!!, Uri.fromFile(File(cacheDir, "user_profile_pic.jpg")))
                        .withOptions(options)
                        .withAspectRatio(1f, 1f)
                        .withMaxResultSize(1024, 1024)
                        .start(this@RegisterActivity)
                }
            }
        })

        if (requestCode == UCrop.REQUEST_CROP) {
            if (data == null) {
                Toasty.error(this, "Error cropping image", Toasty.LENGTH_LONG).show()
                return;
            }

            if (resultCode == RESULT_OK) {
                imageUri = UCrop.getOutput(data);
                ivProfile.setImageURI(imageUri);
            } else if (resultCode == UCrop.RESULT_ERROR) {
                Toasty.error(this, "Error cropping image", Toasty.LENGTH_LONG).show()
                Log.e("Error", "Crop error:" + UCrop.getError(data)!!.message);
            }
        }
    }

    fun selectProfilePic(view: View) {
        easyImage.openChooser(this)
    }

    override fun onDestroy() {
        compositeDisposable.clear()
        super.onDestroy()
    }
}
