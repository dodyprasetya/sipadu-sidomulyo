package com.sipadu.android.ui.profile

import android.app.ProgressDialog
import android.content.Intent
import android.graphics.Bitmap
import android.net.Uri
import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.sipadu.android.R
import com.sipadu.android.model.Profile
import com.sipadu.android.server.ServerManager
import com.sipadu.android.util.AppUtil
import com.sipadu.android.util.RequestUtil
import com.yalantis.ucrop.UCrop
import es.dmoral.toasty.Toasty
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_edit_profile.*
import pl.aprilapps.easyphotopicker.EasyImage
import pl.aprilapps.easyphotopicker.MediaFile
import pl.aprilapps.easyphotopicker.MediaSource
import java.io.File
import java.util.concurrent.TimeUnit

class EditProfileActivity : AppCompatActivity() {

    lateinit var dialog: ProgressDialog

    private val compositeDisposable = CompositeDisposable()

    var profile: Profile? = null

    private var imageUri: Uri? = null
    lateinit var easyImage: EasyImage

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_edit_profile)

        initProgressDialog()

        easyImage = EasyImage.Builder(this).build()

        backIcon.setOnClickListener {
            onBackPressed()
        }

        btSubmit.setOnClickListener {
            submit()
        }

        profile = AppUtil.getProfile(this)
        profile?.let {
            etFullname.setText(it.fullname)
            etEmail.setText(it.email)
            etPhone.setText(it.phone)
            etAddress.setText(it.address)
            etNik.setText(it.nik)
            etKK.setText(it.no_kk)
            etRT.setText(it.rt)
            etRW.setText(it.rw)

            Glide.with(this)
                .load(it.profile_pic)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .circleCrop()
                .into(ivProfile)
        }
    }

    private fun initProgressDialog() {
        dialog = ProgressDialog(this);
        dialog.setMessage("Mohon tunggu..");
        dialog.setIndeterminate(true);
        dialog.setCanceledOnTouchOutside(false);
        dialog.setCancelable(false);
    }

    private fun submit() {
        val fullname = etFullname.text.toString()
        val email = etEmail.text.toString()
        val address = etAddress.text.toString()
        val phone = etPhone.text.toString()
        val nik = etNik.text.toString()
        val kk = etKK.text.toString()
        val rt = etRT.text.toString()
        val rw = etRW.text.toString()

        if (
            !TextUtils.isEmpty(fullname) &&
            !TextUtils.isEmpty(kk) &&
            !TextUtils.isEmpty(rt) &&
            !TextUtils.isEmpty(rw) &&
            !TextUtils.isEmpty(nik) &&
            !TextUtils.isEmpty(phone)
        ) {
            postUser()
        } else {
            Toasty.info(this, "Mohon isi semua data bertanda *", Toasty.LENGTH_LONG).show()
        }
    }

    private fun postUser() {
        val fullname = etFullname.text.toString();
        val email = etEmail.text.toString();
        val address = etAddress.text.toString();
        val phone = etPhone.text.toString();
        val nik = etNik.text.toString();
        val position = profile?.position ?: "Warga"
        val id = profile?.id ?: "0"
        val kk = etKK.text.toString()
        val rt = etRT.text.toString().toInt().toString() // to strip 0 on the front
        val rw = etRW.text.toString().toInt().toString() // to strip 0 on the front

        val user = hashMapOf(
            "user_id" to id,
            "email" to email,
            "fullname" to fullname,
            "address" to address,
            "nik" to nik,
            "no_kk" to kk,
            "rt" to rt,
            "rw" to rw,
            "phone" to phone,
            "position" to position
        )

        val disposable = ServerManager.getInstance()
            .service.updateProfile(user)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .debounce(400, TimeUnit.MILLISECONDS)
            .doOnSubscribe { dialog.show() }
            .doOnTerminate { dialog.dismiss() }
            .subscribe(
                {
                    Toasty.success(this, "Update profil berhasil", Toasty.LENGTH_LONG).show()

                    profile?.let {
                        it.fullname = fullname
                        it.address = address
                        it.phone = phone
                        it.email = email
                        it.nik = nik
                        it.no_kk = kk
                        it.rt = rt
                        it.rw = rw

                        AppUtil.saveProfile(this, it)
                    }

                    finish()
                },
                {
                    Toasty.error(this, "Update profil gagal, silahkan coba lagi", Toasty.LENGTH_LONG).show()
                }
            )

        compositeDisposable.add(disposable)
    }

    override fun onDestroy() {
        compositeDisposable.clear()
        super.onDestroy()
    }

    fun selectProfilePic(view: View) {
        easyImage.openChooser(this)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        easyImage.handleActivityResult(requestCode, resultCode, data, this, object : EasyImage.Callbacks {
            override fun onImagePickerError(error: Throwable, source: MediaSource) {
                Toasty.error(this@EditProfileActivity, "Error dalam mengambil gambar", Toasty.LENGTH_LONG).show()
                error.printStackTrace()
            }

            override fun onCanceled(source: MediaSource) {
            }

            override fun onMediaFilesPicked(imageFiles: Array<MediaFile>, source: MediaSource) {
                if (imageFiles.size > 0) {

                    imageUri = Uri.fromFile(imageFiles[0].file)

                    val options = UCrop.Options()
                    options.setCompressionFormat(Bitmap.CompressFormat.JPEG)
                    options.setCompressionQuality(80)
                    options.setShowCropGrid(true)

                    UCrop.of(imageUri!!, Uri.fromFile(File(cacheDir, "user_profile_pic.jpg")))
                        .withOptions(options)
                        .withAspectRatio(1f, 1f)
                        .withMaxResultSize(1024, 1024)
                        .start(this@EditProfileActivity)
                }
            }
        })

        if (requestCode == UCrop.REQUEST_CROP) {
            if (data == null) {
                Toasty.error(this, "Error cropping image", Toasty.LENGTH_LONG).show()
                return;
            }

            if (resultCode == RESULT_OK) {
                imageUri = UCrop.getOutput(data)
                ivProfile.setImageURI(imageUri)

                updateProfilePic()

            } else if (resultCode == UCrop.RESULT_ERROR) {
                Toasty.error(this, "Error cropping image", Toasty.LENGTH_LONG).show()
                Log.e("Error", "Crop error:" + UCrop.getError(data)!!.message);
            }
        }
    }

    private fun updateProfilePic() {
        if (imageUri != null) {
            val id = profile?.id ?: "0"

            val user = hashMapOf(
                "user_id" to RequestUtil.getBody(id)
            )
            val fileRequestBody = RequestUtil.getFileBody(imageUri!!)

            val disposable = ServerManager.getInstance()
                .service.updateProfilePic(user, fileRequestBody)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .debounce(400, TimeUnit.MILLISECONDS)
                .doOnSubscribe { dialog.show() }
                .doOnTerminate { dialog.dismiss() }
                .subscribe(
                    {
                        if (it.url.equals("Gagal Upload") || it.url.equals("No Image")) {
                            Toasty.error(this, "Upload gambar gagal", Toasty.LENGTH_LONG).show()
                        } else if (it.url.equals("Gagal")) {
                            Toasty.error(this, "Update gagal", Toasty.LENGTH_LONG).show()
                        } else {
                            Toasty.success(
                                this,
                                "Update gambar profil berhasil",
                                Toasty.LENGTH_LONG
                            ).show()

                            Glide.with(this)
                                .load(it.url)
                                .diskCacheStrategy(DiskCacheStrategy.ALL)
                                .circleCrop()
                                .into(ivProfile)

                            val newProfile = profile
                            newProfile?.profile_pic = it.url
                            AppUtil.saveProfile(this, newProfile!!)
                        }
                    },
                    {
                        Toasty.error(
                            this,
                            "Update gagal, silahkan coba lagi",
                            Toasty.LENGTH_LONG
                        ).show()
                    }
                )

            compositeDisposable.add(disposable)
        }
    }
}
