package com.sipadu.android.ui.website

import android.os.Bundle
import android.webkit.WebResourceRequest
import android.webkit.WebView
import androidx.appcompat.app.AppCompatActivity
import com.sipadu.android.R
import io.reactivex.disposables.CompositeDisposable
import kotlinx.android.synthetic.main.activity_letter.*
import android.webkit.WebViewClient

class WebsiteActivity : AppCompatActivity() {

    private val compositeDisposable = CompositeDisposable()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_website)

        backIcon.setOnClickListener {
            onBackPressed()
        }

        webView.apply {
            settings.javaScriptEnabled = true
            settings.javaScriptCanOpenWindowsAutomatically = true
            settings.domStorageEnabled = true

            webView.webViewClient = object : WebViewClient() {
                override fun shouldOverrideUrlLoading(
                    view: WebView?,
                    request: WebResourceRequest?
                ): Boolean {
                    val url = request?.url.toString()
                    view?.loadUrl(url);

                    return super.shouldOverrideUrlLoading(view, request)
                }
            }

            loadUrl("https://wisatasidomulyo.com")
        }
    }

    override fun onDestroy() {
        compositeDisposable.clear()
        super.onDestroy()
    }
}
