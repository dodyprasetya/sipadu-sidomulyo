package com.sipadu.android.ui.report

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.sipadu.android.R
import com.sipadu.android.model.Profile
import com.sipadu.android.util.AppUtil
import com.sipadu.android.util.DateUtil
import kotlinx.android.synthetic.main.activity_report_detail.*

class ReportDetailActivity : AppCompatActivity(), OnMapReadyCallback {

    private var id: String = "0"
    private var fullname: String = ""
    private var title: String = ""
    private var photo: String = ""
    private var lat: String = "0"
    private var lng: String = "0"
    private var content: String = "0"
    private var createdDate: String = "0"

    private var dateTime: String = ""

    private lateinit var mMap: GoogleMap
    private var profile: Profile? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_report_detail)

        intent.extras?.let {
            id = it.getString("id", "0")
            fullname = it.getString("fullname", "")
            title = it.getString("title", "")
            photo = it.getString("photo", "")
            content = it.getString("content", "")
            lat = it.getString("lat", "0")
            lng = it.getString("lng", "0")
            createdDate = it.getString("created_date", "")
        }

        profile = AppUtil.getProfile(this)

        val time = DateUtil.convertDateString(
            date = createdDate,
            inputFormat = DateUtil.MYSQL_DATE_TIME_FORMAT,
            outputFormat = DateUtil.SIMPLE_TIME_FORMAT
        )
        val date = DateUtil.getDateWithShortMonthName(
            context = this,
            date = createdDate,
            inputFormat = DateUtil.MYSQL_DATE_TIME_FORMAT
        )
        dateTime = "$time, $date"

        backIcon.setOnClickListener {
            onBackPressed()
        }

        val mapFragment = supportFragmentManager
            .findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)

        initData()
    }

    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap

        val position = LatLng(lat.toDouble(), lng.toDouble())
        mMap.addMarker(
            MarkerOptions()
                .position(position)
                .title("$fullname")
                .snippet("Pada $dateTime")
        )?.showInfoWindow()

        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(position, 16f))
    }

    private fun initData() {
        Glide.with(this)
            .load(photo)
            .diskCacheStrategy(DiskCacheStrategy.ALL)
            .centerCrop()
            .into(ivPhoto)

        tvName.text = fullname
        tvDate.text = dateTime
        tvDetail.text = if (content == "") "-" else content
    }
}
