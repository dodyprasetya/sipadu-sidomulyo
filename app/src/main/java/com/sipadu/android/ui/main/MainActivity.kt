package com.sipadu.android.ui.main

import android.content.Intent
import android.os.Build
import android.os.Bundle
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.google.android.gms.location.*
import com.google.firebase.messaging.FirebaseMessaging
import com.sipadu.android.R
import com.sipadu.android.app.Constants
import com.sipadu.android.model.Profile
import com.sipadu.android.server.ServerManager
import com.sipadu.android.ui.fragments.complaint.ComplaintFragment
import com.sipadu.android.ui.fragments.home.HomeFragment
import com.sipadu.android.ui.fragments.incident.IncidentFragment
import com.sipadu.android.ui.fragments.profile.ProfileFragment
import com.sipadu.android.ui.login.LoginActivity
import com.sipadu.android.util.AlarmUtil
import com.sipadu.android.util.AppUtil
import com.sipadu.android.util.PermissionUtil
import com.sipadu.android.util.PrefUtil
import es.dmoral.toasty.Toasty
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_main.*
import java.lang.Long
import java.util.concurrent.TimeUnit

class MainActivity : AppCompatActivity() {

    private val compositeDisposable = CompositeDisposable()

    var profile: Profile? = null

    lateinit var home: Fragment
    lateinit var complaint: Fragment
    lateinit var incident: Fragment
    lateinit var profileFragment: Fragment

    private var selectedTab: String = "home"

    lateinit var alertDialog: AlertDialog

    private lateinit var fusedLocationClient: FusedLocationProviderClient

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        intent.extras?.let {
            selectedTab = it.getString("selected_tab", "home")
        }

        initAlertDialog()

        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this)

        profile = AppUtil.getProfile(this)

        home = HomeFragment()
        complaint = ComplaintFragment()
        incident = IncidentFragment()
        profileFragment = ProfileFragment()

        loadFragment(
            if (selectedTab == "complaint") complaint
            else if (selectedTab == "incident") incident
            else home
        )
        bottomNavView.selectedItemId =
            if (selectedTab == "complaint") R.id.navigation_complaint
            else if (selectedTab == "incident") R.id.navigation_incident
            else R.id.navigation_home

        bottomNavView.setOnItemSelectedListener { item ->
            when (item.itemId) {
                R.id.navigation_home -> {
                    loadFragment(home)
                    true
                }
                R.id.navigation_complaint -> {
                    loadFragment(complaint)
                    true
                }
                R.id.navigation_incident -> {
                    loadFragment(incident)
                    true
                }
                R.id.navigation_profile -> {
                    loadFragment(profileFragment)
                    true
                }
                else -> false
            }
        }

        checkUser()

        FirebaseMessaging.getInstance().subscribeToTopic("info")
    }

    private fun initAlertDialog() {
        alertDialog = AlertDialog.Builder(this)
            .setTitle("Location Permission")
            .setMessage("Sipadu Sidomulyo collects location data to enable tracking employee even when the app is closed or not in use.")
            .setCancelable(false)
            .setPositiveButton("Approve") { dialog, position ->
                alertDialog.dismiss()

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
                    PermissionUtil.askBackgroundLocationPermissions(this@MainActivity)
                } else {
                    PermissionUtil.askLocationPermissions(this)
                }
            }
            .setNegativeButton("Deny") { dialog, position ->
                alertDialog.dismiss()
                Toasty.info(this,
                    "Anda sudah menolak izin, silahkan berikan izin untuk melanjutkan",
                    Toasty.LENGTH_LONG).show()
                finish()
            }
            .create()
    }

    override fun onStart() {
        super.onStart()

        askLocationPermissions()
        startLocationTracker()
    }

    override fun onResume() {
        super.onResume()

        PrefUtil.read(this, Constants.PREF_IS_LOCATION_TRACKER_RUNNING, "")
            .also {
                if (!it.equals("yes")) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
                        if (!PermissionUtil.isBackgroundLocationPermissionGranted(this)) return
                    } else {
                        if (!PermissionUtil.isLocationPermissionGranted(this)) return
                    }

                    startLocationTracker()
                }
            }
    }

    private fun askLocationPermissions() {
        if (profile?.type == "staff") {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
                if (PermissionUtil.isBackgroundLocationPermissionGranted(this)) return
            } else {
                if (PermissionUtil.isLocationPermissionGranted(this)) return
            }

            alertDialog.show()
        }
    }

    private fun shouldUpdateLocation(): Boolean {
        val lastUpdateLocation = PrefUtil.read(this, Constants.PREF_LAST_UPDATE_LOCATION, "0")
        val diff = System.currentTimeMillis() / 1000 - Long.parseLong(lastUpdateLocation!!)
        val interval = Constants.LOCATION_UPDATE_INTERVAL / 1000

        return diff > interval
    }

    private fun startLocationTracker() {
        if (profile?.type == "staff") {
            // checking location permission
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
                if (!PermissionUtil.isBackgroundLocationPermissionGranted(this)) return
            } else {
                if (!PermissionUtil.isLocationPermissionGranted(this)) return
            }

            if (shouldUpdateLocation()) getCurrentLocation()

            startLocationIntervalRequest()
        } else {
            cancelLocationRequest()
        }
    }

    private fun startLocationIntervalRequest() {
        AlarmUtil.startLocationAlarm(this)
        PrefUtil.write(this, Constants.PREF_IS_LOCATION_TRACKER_RUNNING, "yes")
    }

    private fun cancelLocationRequest() {
        AlarmUtil.cancelLocationAlarm(this)
        PrefUtil.remove(this, Constants.PREF_IS_LOCATION_TRACKER_RUNNING)
    }

    private fun getCurrentLocation() {
        // checking location permission
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            if (!PermissionUtil.isBackgroundLocationPermissionGranted(this)) return
        } else {
            if (!PermissionUtil.isLocationPermissionGranted(this)) return
        }

        fusedLocationClient.lastLocation
            .addOnSuccessListener { location ->
                // getting the last known or current location
                val latitude = location.latitude
                val longitude = location.longitude

                updateStaffLocation(latitude.toString(), longitude.toString())
            }
            .addOnFailureListener {}
    }

    private fun loadFragment(fragment: Fragment) {
        val transaction = supportFragmentManager.beginTransaction()
        transaction.replace(R.id.frameLayout, fragment)
        transaction.commit()
    }

    private fun updateFcmToken() {
        val shouldUpdateFcm = PrefUtil.read(this, Constants.PREF_SHOULD_UPDATE_FCM_TOKEN, "")

        if (shouldUpdateFcm == "yes") {
            val token = PrefUtil.read(this, Constants.PREF_FCM_TOKEN, "")

            profile?.let {
                val disposable = ServerManager.getInstance()
                    .service.updateFcmToken(it.id, token!!)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .debounce(400, TimeUnit.MILLISECONDS)
                    .subscribe(
                        {
                            PrefUtil.remove(this, Constants.PREF_SHOULD_UPDATE_FCM_TOKEN)
                        },
                        {}
                    )

                compositeDisposable.add(disposable)
            }
        }
    }

    private fun checkUser() {
        val id = profile?.id ?: "0"

        val disposable = ServerManager.getInstance()
            .service.checkUser(id)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .debounce(400, TimeUnit.MILLISECONDS)
            .subscribe({
                if (it.equals("Valid")) {
                    updateFcmToken()
                } else {
                    Toasty.info(this,
                        "Akun Anda sudah dinonaktifkan, silahkan hubungi Admin untuk info lebih lanjut",
                        Toasty.LENGTH_LONG).show()

                    // force logout jika user sudah tidak aktif
                    AlarmUtil.cancelLocationAlarm(this)
                    PrefUtil.remove(this, Constants.PREF_IS_LOCATION_TRACKER_RUNNING)
                    PrefUtil.remove(this, Constants.PREF_PROFILE)

                    val intent = Intent(this, LoginActivity::class.java)
                    intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK
                    startActivity(intent)
                }
            }, {})

        compositeDisposable.add(disposable)
    }

    private fun updateStaffLocation(lat: String, lng: String) {
        profile?.let {
            val disposable = ServerManager.getInstance()
                .service.trackLocation(it.id, lat, lng)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .debounce(400, TimeUnit.MILLISECONDS)
                .subscribe(
                    {
                        val time = (System.currentTimeMillis() / 1000).toString()
                        PrefUtil.write(this, Constants.PREF_LAST_UPDATE_LOCATION, time)
                    },
                    {}
                )

            compositeDisposable.add(disposable)
        }
    }

    override fun onDestroy() {
        compositeDisposable.clear()
        super.onDestroy()
    }
}
