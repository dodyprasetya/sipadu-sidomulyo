package com.sipadu.android.ui.report

import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import com.sipadu.android.R
import com.sipadu.android.adapter.ReportAdapter
import com.sipadu.android.model.Report
import com.sipadu.android.model.Profile
import com.sipadu.android.server.ServerManager
import com.sipadu.android.util.AppUtil
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_report.*
import java.util.concurrent.TimeUnit

class ReportActivity : AppCompatActivity(), ReportAdapter.Listener {

    private val compositeDisposable = CompositeDisposable()

    val itemList = ArrayList<Report>()
    lateinit var adapter: ReportAdapter

    private var profile: Profile? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_report)

        backIcon.setOnClickListener {
            onBackPressed()
        }

        profile = AppUtil.getProfile(this)

        profile?.let {
            if (it.type == "staff") {
                tvHeaderTitle.text = "Laporan Kegiatan Anda"
            } else {
                tvHeaderTitle.text = "Laporan Kegiatan Pegawai"
            }
        }

        adapter = ReportAdapter()
        adapter.listener = this
        recyclerView.adapter = adapter

        refreshLayout.setOnRefreshListener {
            loadData()
        }

        iconAdd.setOnClickListener {
            startActivity(Intent(this, CreateReportActivity::class.java))
        }
    }

    override fun onResume() {
        super.onResume()
        loadData()
    }

    private fun loadData() {
        val id = profile?.id ?: "0"
        val type = profile?.type ?: ""

        val observable: Observable<List<Report>>

        if (type == "staff") {
            observable = ServerManager.getInstance().service.loadUserReports(id)
        } else {
            observable = ServerManager.getInstance().service.loadAllReports()
        }

        val disposable = observable
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .debounce(400, TimeUnit.MILLISECONDS)
            .doOnSubscribe { refreshLayout.isRefreshing = true }
            .doOnTerminate { refreshLayout.isRefreshing = false }
            .subscribe(
                {
                    itemList.clear()
                    itemList.addAll(it)
                    adapter.updateData(itemList)
                },
                {
                    Log.d("Error", "Error getting datas.", it)
                }
            )

        compositeDisposable.add(disposable)
    }

    override fun onDestroy() {
        compositeDisposable.clear()
        super.onDestroy()
    }

    override fun onItemSelected(item: Report) {
        val intent = Intent(this, ReportDetailActivity::class.java)

        intent.apply {
            with(item) {
                putExtra("id", id)
                putExtra("title", title)
                putExtra("content", content)
                putExtra("photo", photo)
                putExtra("lat", lat)
                putExtra("lng", lng)
                putExtra("fullname", fullname)
                putExtra("created_date", created_date)
            }
        }

        startActivity(intent)
    }
}
