package com.sipadu.android.ui.complaint

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.sipadu.android.R
import com.sipadu.android.util.DateUtil
import io.reactivex.disposables.CompositeDisposable
import kotlinx.android.synthetic.main.activity_complaint_detail.*

class ComplaintDetailActivity : AppCompatActivity() {

    private val compositeDisposable = CompositeDisposable()

    private var id: String = "0"
    private var title: String = ""
    private var content: String = ""
    private var photo: String = ""
    private var fullname: String = ""
    private var user_id: String = ""
    private var created_date: String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_complaint_detail)

        intent.extras?.let {
            id = it.getString("id", "0")
            fullname = it.getString("fullname", "")
            title = it.getString("title", "")
            content = it.getString("content", "")
            photo = it.getString("photo", "")
            user_id = it.getString("user_id", "")
            created_date = it.getString("created_date", "")

            initData()
        }

        backIcon.setOnClickListener {
            onBackPressed()
        }
    }

    override fun onDestroy() {
        compositeDisposable.clear()
        super.onDestroy()
    }

    private fun initData() {
        tvName.text = fullname
        tvTitle.text = title
        tvContent.text = content
        val time = DateUtil.convertDateString(
            date = created_date,
            inputFormat = DateUtil.MYSQL_DATE_TIME_FORMAT,
            outputFormat = DateUtil.SIMPLE_TIME_FORMAT
        )
        val date = DateUtil.getDateWithShortMonthName(
            context = this,
            date = created_date,
            inputFormat = DateUtil.MYSQL_DATE_TIME_FORMAT
        )
        tvDate.text = "${time}, ${date}"

        if (photo != "") {
            cvPhoto.visibility = View.VISIBLE
            Glide.with(this)
                .load(photo)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .centerCrop()
                .into(ivPhoto)
        } else {
            cvPhoto.visibility = View.GONE
        }
    }
}
