package com.sipadu.android.ui.fragments.profile

import android.app.ProgressDialog
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.sipadu.android.R
import com.sipadu.android.app.Constants
import com.sipadu.android.model.Profile
import com.sipadu.android.server.ServerManager
import com.sipadu.android.ui.login.LoginActivity
import com.sipadu.android.ui.profile.ChangePasswordActivity
import com.sipadu.android.ui.profile.EditProfileActivity
import com.sipadu.android.util.AlarmUtil
import com.sipadu.android.util.AppUtil
import com.sipadu.android.util.PrefUtil
import es.dmoral.toasty.Toasty
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.fragment_profile.*
import java.util.concurrent.TimeUnit

class ProfileFragment : Fragment() {

    private val compositeDisposable = CompositeDisposable()

    lateinit var dialog: ProgressDialog

    var profile: Profile? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_profile, container, false)
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initProgressDialog()

        tvEditProfile.setOnClickListener {
            startActivity(Intent(requireContext(), EditProfileActivity::class.java))
        }

        tvChangePassword.setOnClickListener {
            startActivity(Intent(requireContext(), ChangePasswordActivity::class.java))
        }

        tvWhatsapp.setOnClickListener {
            val url = "https://api.whatsapp.com/send?phone=${Constants.ADMIN_WHATSAPP_NUMBER}"
            val intent = Intent(Intent.ACTION_VIEW)
            intent.data = Uri.parse(url)
            startActivity(intent)
        }

        btSignOut.setOnClickListener {
            logout()
        }
    }

    private fun initProgressDialog() {
        dialog = ProgressDialog(requireContext())
        dialog.setMessage("Please wait..")
        dialog.setIndeterminate(true)
        dialog.setCanceledOnTouchOutside(false)
        dialog.setCancelable(false)
    }

    override fun onResume() {
        super.onResume()

        profile = AppUtil.getProfile(requireContext())
        profile?.let {
            Glide.with(requireContext())
                .load(it.profile_pic)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .circleCrop()
                .into(ivProfile)

            tvName.text = it.fullname
            tvPosition.text = it.position
        }
    }

    override fun onDestroy() {
        compositeDisposable.clear()
        super.onDestroy()
    }

    fun logout() {
        profile?.let {
            val disposable = ServerManager.getInstance()
                .service.updateFcmToken(it.id, "")
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .debounce(400, TimeUnit.MILLISECONDS)
                .doOnSubscribe { dialog.show() }
                .doOnTerminate { dialog.dismiss() }
                .subscribe(
                    {
                        AlarmUtil.cancelLocationAlarm(requireContext())
                        PrefUtil.remove(requireContext(), Constants.PREF_IS_LOCATION_TRACKER_RUNNING)
                        PrefUtil.remove(requireContext(), Constants.PREF_PROFILE)

                        val intent = Intent(requireContext(), LoginActivity::class.java)
                        intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK
                        startActivity(intent)
                    },
                    {
                        Toasty.error(requireContext(), "Logout gagal, silahkan coba lagi", Toasty.LENGTH_LONG).show()
                    }
                )

            compositeDisposable.add(disposable)
        }
    }
}