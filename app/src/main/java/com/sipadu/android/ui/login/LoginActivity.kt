package com.sipadu.android.ui.login

import android.app.ProgressDialog
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.text.TextUtils
import androidx.appcompat.app.AppCompatActivity
import com.google.gson.Gson
import com.sipadu.android.R
import com.sipadu.android.app.Constants
import com.sipadu.android.model.Profile
import com.sipadu.android.server.ServerManager
import com.sipadu.android.ui.main.MainActivity
import com.sipadu.android.ui.register.RegisterActivity
import com.sipadu.android.util.AppUtil
import com.sipadu.android.util.PrefUtil
import es.dmoral.toasty.Toasty
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_login.*
import java.util.concurrent.TimeUnit

class LoginActivity : AppCompatActivity() {

    private val compositeDisposable = CompositeDisposable()

    lateinit var dialog: ProgressDialog

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        if (AppUtil.isLogin(this)) {
            startActivity(Intent(this, MainActivity::class.java))
            finish()
        }

        setContentView(R.layout.activity_login)

        btLogin.setOnClickListener {
            signIn()
        }

        tvRegister.setOnClickListener {
            startActivity(Intent(this, RegisterActivity::class.java))
        }

        tvForgotPassword.setOnClickListener {
            val url = "https://api.whatsapp.com/send?phone=${Constants.ADMIN_WHATSAPP_NUMBER}"
            val intent = Intent(Intent.ACTION_VIEW)
            intent.data = Uri.parse(url)
            startActivity(intent)
        }

        initProgressDialog()
    }

    private fun initProgressDialog() {
        dialog = ProgressDialog(this);
        dialog.setMessage("Mohon tunggu..");
        dialog.setIndeterminate(true);
        dialog.setCanceledOnTouchOutside(false);
        dialog.setCancelable(false);
    }

    private fun signIn() {
        val username = etUsername.text.toString();
        val password = etPassword.text.toString();

        if (
            !TextUtils.isEmpty(username) &&
            !TextUtils.isEmpty(password)
        ) {

            val disposable = ServerManager.getInstance()
                .service.login(username, password)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .debounce(400, TimeUnit.MILLISECONDS)
                .doOnSubscribe { dialog.show() }
                .doOnTerminate { dialog.dismiss() }
                .subscribe(
                    {
                        checkLoginData(it)
                    },
                    {
                        Toasty.error(this, "Login gagal", Toasty.LENGTH_LONG).show()
                    }
                )

            compositeDisposable.add(disposable)

        } else {
            Toasty.info(this, "Mohon isi semua field", Toasty.LENGTH_LONG).show()
        }
    }

    fun checkLoginData(profile: Profile) {
        if (profile.active == "0") {
            Toasty.info(this, "Akun Anda belum aktif, silahkan tunggu persetujuan Admin",
                Toasty.LENGTH_LONG).show()
            return
        }

        val profileJson = Gson().toJson(profile)
        PrefUtil.write(this, Constants.PREF_PROFILE, profileJson)

        // should update fcm to server
        PrefUtil.write(this, Constants.PREF_SHOULD_UPDATE_FCM_TOKEN, "yes")

        startActivity(Intent(this, MainActivity::class.java))
        finish()
    }

    override fun onDestroy() {
        compositeDisposable.clear()
        super.onDestroy()
    }
}
