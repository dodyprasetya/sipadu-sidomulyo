package com.sipadu.android.ui.info

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.sipadu.android.R
import com.sipadu.android.adapter.InfoAdapter
import com.sipadu.android.model.Info
import com.sipadu.android.model.Profile
import com.sipadu.android.server.ServerManager
import com.sipadu.android.util.AppUtil
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_info.*
import java.util.concurrent.TimeUnit

class InfoActivity : AppCompatActivity(), InfoAdapter.Listener {

    private val compositeDisposable = CompositeDisposable()

    val itemList = ArrayList<Info>()
    lateinit var adapter: InfoAdapter

    private var profile: Profile? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_info)

        backIcon.setOnClickListener {
            onBackPressed()
        }

        profile = AppUtil.getProfile(this)

        adapter = InfoAdapter()
        adapter.listener = this
        recyclerView.adapter = adapter

        refreshLayout.setOnRefreshListener {
            loadData()
        }

        profile?.let {
            if (it.type == "admin") {
                iconAdd.visibility = View.VISIBLE
            } else {
                iconAdd.visibility = View.GONE
            }
        }

        iconAdd.setOnClickListener {
            startActivity(Intent(this, CreateInfoActivity::class.java))
        }
    }

    override fun onResume() {
        super.onResume()
        loadData()
    }

    private fun loadData() {
        val disposable = ServerManager.getInstance()
            .service.loadInfo()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .debounce(400, TimeUnit.MILLISECONDS)
            .doOnSubscribe { refreshLayout.isRefreshing = true }
            .doOnTerminate { refreshLayout.isRefreshing = false }
            .subscribe(
                {
                    itemList.clear()
                    itemList.addAll(it)
                    adapter.updateData(itemList)
                },
                {
                    Log.d("Error", "Error getting datas.", it)
                }
            )

        compositeDisposable.add(disposable)
    }

    override fun onDestroy() {
        compositeDisposable.clear()
        super.onDestroy()
    }

    override fun onItemSelected(item: Info) {
        val intent = Intent(this, InfoDetailActivity::class.java)

        intent.apply {
            with(item) {
                putExtra("id", id)
                putExtra("title", title)
                putExtra("content", content)
                putExtra("photo", photo)
                putExtra("user_id", user_id)
                putExtra("fullname", fullname)
                putExtra("created_date", created_date)
            }
        }

        startActivity(intent)
    }
}
