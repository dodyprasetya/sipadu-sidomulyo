package com.sipadu.android.ui.report

import android.app.ProgressDialog
import android.content.Intent
import android.graphics.Bitmap
import android.net.Uri
import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.sipadu.android.R
import com.sipadu.android.model.Profile
import com.sipadu.android.server.ServerManager
import com.sipadu.android.util.AppUtil
import com.sipadu.android.util.PermissionUtil
import com.sipadu.android.util.RequestUtil
import com.yalantis.ucrop.UCrop
import es.dmoral.toasty.Toasty
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_create_report.*
import pl.aprilapps.easyphotopicker.EasyImage
import pl.aprilapps.easyphotopicker.MediaFile
import pl.aprilapps.easyphotopicker.MediaSource
import java.io.File
import java.util.concurrent.TimeUnit

class CreateReportActivity : AppCompatActivity() {

    lateinit var dialog: ProgressDialog

    private val compositeDisposable = CompositeDisposable()

    var profile: Profile? = null

    private var imageUri: Uri? = null
    lateinit var easyImage: EasyImage;

    private var latitude: Double = 0.0
    private var longitude: Double = 0.0

    private lateinit var fusedLocationClient: FusedLocationProviderClient
    lateinit var alertDialog: AlertDialog

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_create_report)

        backIcon.setOnClickListener {
            onBackPressed()
        }

        easyImage = EasyImage.Builder(this).build()
        profile = AppUtil.getProfile(this)
        initProgressDialog()

        initAlertDialog()
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this)
        getCurrentLocation()

        btSubmit.setOnClickListener {
            submit()
        }

        ivPhoto.setOnClickListener {
            easyImage.openChooser(this)
        }
    }

    private fun getCurrentLocation() {
        // checking location permission
        if (!PermissionUtil.isLocationPermissionGranted(this)) return

        fusedLocationClient.lastLocation
            .addOnSuccessListener { location ->
                // getting the last known or current location
                latitude = location.latitude
                longitude = location.longitude
            }
            .addOnFailureListener {}
    }

    override fun onStart() {
        super.onStart()
        PermissionUtil.askStorageAndCameraPermissions(this)

        if (!PermissionUtil.isLocationPermissionGranted(this)) {
            alertDialog.show()
        }
    }

    private fun initProgressDialog() {
        dialog = ProgressDialog(this);
        dialog.setMessage("Mohon tunggu..");
        dialog.setIndeterminate(true);
        dialog.setCanceledOnTouchOutside(false);
        dialog.setCancelable(false);
    }

    private fun initAlertDialog() {
        alertDialog = AlertDialog.Builder(this)
            .setTitle("Location Permission")
            .setMessage("Sipadu Sidomulyo collects location data to enable creating incident report or laporan kegiatan even when the app is closed or not in use.")
            .setCancelable(false)
            .setPositiveButton("Approve") { dialog, position ->
                alertDialog.dismiss()

                PermissionUtil.askLocationPermissions(this)
            }
            .setNegativeButton("Deny") { dialog, position ->
                alertDialog.dismiss()
                Toasty.info(this,
                    "Anda sudah menolak izin, silahkan berikan izin untuk melanjutkan",
                    Toasty.LENGTH_LONG).show()
                finish()
            }
            .create()
    }

    private fun submit() {
        val title = etTitle.text.toString()
        val content = etContent.text.toString()
        val fullname = profile?.fullname ?: ""
        val userId = profile?.id ?: "0"

        if (
            !TextUtils.isEmpty(title) &&
            !TextUtils.isEmpty(content)
        ) {
            val request = hashMapOf(
                "title" to RequestUtil.getBody(title),
                "content" to RequestUtil.getBody(content),
                "fullname" to RequestUtil.getBody(fullname),
                "user_id" to RequestUtil.getBody(userId),
                "lat" to RequestUtil.getBody(latitude.toString()),
                "lng" to RequestUtil.getBody(longitude.toString()),
            )

            val observable: Observable<String>

            if (imageUri != null) {
                val fileRequestBody = RequestUtil.getFileBody(imageUri!!)
                observable = ServerManager.getInstance().service.createEmployeeReport(request, fileRequestBody)
            } else {
                observable = ServerManager.getInstance().service.createEmployeeReportWithoutImage(request)
            }

            val disposable = observable
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .debounce(400, TimeUnit.MILLISECONDS)
                .doOnSubscribe { dialog.show() }
                .doOnTerminate { dialog.dismiss() }
                .subscribe(
                    {
                        if (it.equals("Berhasil")) {
                            Toasty.success(this, "Buat laporan berhasil", Toasty.LENGTH_LONG).show()
                            finish()
                        } else if (it.equals("Gagal Upload") || it.equals("No Image")) {
                            Toasty.error(this, "Upload gambar gagal, silahkan coba lagi", Toasty.LENGTH_LONG).show()
                        } else {
                            Toasty.error(this, "Buat laporan gagal, silahkan coba lagi", Toasty.LENGTH_LONG).show()
                        }
                    },
                    {
                        Toasty.error(this, "Buat laporan gagal, silahkan coba lagi", Toasty.LENGTH_LONG).show()
                    }
                )

            compositeDisposable.add(disposable)
        } else {
            Toasty.info(this, "Mohon isi semua data", Toasty.LENGTH_LONG).show()
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        easyImage.handleActivityResult(requestCode, resultCode, data, this, object : EasyImage.Callbacks {
            override fun onImagePickerError(error: Throwable, source: MediaSource) {
                Toasty.error(this@CreateReportActivity, "Error dalam mengambil gambar", Toasty.LENGTH_LONG).show()
                error.printStackTrace()
            }

            override fun onCanceled(source: MediaSource) {
            }

            override fun onMediaFilesPicked(imageFiles: Array<MediaFile>, source: MediaSource) {
                if (imageFiles.size > 0) {

                    imageUri = Uri.fromFile(imageFiles[0].file)

                    val options = UCrop.Options()
                    options.setCompressionFormat(Bitmap.CompressFormat.JPEG)
                    options.setCompressionQuality(80)
                    options.setShowCropGrid(true)

                    UCrop.of(imageUri!!, Uri.fromFile(File(cacheDir, "laporan_pegawai.jpg")))
                        .withOptions(options)
                        .withAspectRatio(1f, 1f)
                        .withMaxResultSize(1024, 1024)
                        .start(this@CreateReportActivity)
                }
            }
        })

        if (requestCode == UCrop.REQUEST_CROP) {
            if (data == null) {
                Toasty.error(this, "Error cropping image", Toasty.LENGTH_LONG).show()
                return;
            }

            if (resultCode == RESULT_OK) {
                imageUri = UCrop.getOutput(data);
                ivPhoto.setImageURI(imageUri);
            } else if (resultCode == UCrop.RESULT_ERROR) {
                Toasty.error(this, "Error cropping image", Toasty.LENGTH_LONG).show()
                Log.e("Error", "Crop error:" + UCrop.getError(data)!!.message);
            }
        }
    }

    override fun onDestroy() {
        compositeDisposable.clear()
        super.onDestroy()
    }
}
