package com.sipadu.android.ui.user

import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import com.sipadu.android.R
import com.sipadu.android.adapter.UserAdapter
import com.sipadu.android.model.Profile
import com.sipadu.android.server.ServerManager
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_user.*
import java.util.concurrent.TimeUnit

class UserActivity : AppCompatActivity(), UserAdapter.Listener {

    private val compositeDisposable = CompositeDisposable()

    val itemList = ArrayList<Profile>()
    lateinit var adapter: UserAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_user)

        backIcon.setOnClickListener {
            onBackPressed()
        }

        adapter = UserAdapter()
        adapter.listener = this
        recyclerView.adapter = adapter

        refreshLayout.setOnRefreshListener {
            loadData()
        }
    }

    override fun onResume() {
        super.onResume()
        loadData()
    }

    private fun loadData() {
        val disposable = ServerManager.getInstance()
            .service.loadAllUsers()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .debounce(400, TimeUnit.MILLISECONDS)
            .doOnSubscribe { refreshLayout.isRefreshing = true }
            .doOnTerminate { refreshLayout.isRefreshing = false }
            .subscribe(
                {
                    itemList.clear()
                    itemList.addAll(it)
                    adapter.updateData(itemList)
                },
                {
                    Log.d("Error", "Error getting datas.", it)
                }
            )

        compositeDisposable.add(disposable)
    }

    override fun onDestroy() {
        compositeDisposable.clear()
        super.onDestroy()
    }

    override fun onItemSelected(item: Profile) {
        val intent = Intent(this, ViewProfileActivity::class.java)
        intent.putExtra("id", item.id)
        intent.putExtra("fullname", item.fullname)
        intent.putExtra("email", item.email)
        intent.putExtra("address", item.address)
        intent.putExtra("phone", item.phone)
        intent.putExtra("profile_pic", item.profile_pic)
        intent.putExtra("type", item.type)
        intent.putExtra("position", item.position)
        intent.putExtra("active", item.active)
        intent.putExtra("nik", item.nik)
        intent.putExtra("kk", item.no_kk)
        intent.putExtra("rt", item.rt)
        intent.putExtra("rw", item.rw)
        startActivity(intent)
    }
}
