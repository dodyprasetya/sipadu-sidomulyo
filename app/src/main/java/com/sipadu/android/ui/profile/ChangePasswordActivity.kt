package com.sipadu.android.ui.profile

import android.app.ProgressDialog
import android.os.Bundle
import android.text.TextUtils
import androidx.appcompat.app.AppCompatActivity
import com.sipadu.android.R
import com.sipadu.android.model.Profile
import com.sipadu.android.server.ServerManager
import com.sipadu.android.util.AppUtil
import es.dmoral.toasty.Toasty
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_change_password.*
import java.util.concurrent.TimeUnit

class ChangePasswordActivity : AppCompatActivity() {

    lateinit var dialog: ProgressDialog

    private val compositeDisposable = CompositeDisposable()

    var profile: Profile? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_change_password)

        initProgressDialog()

        backIcon.setOnClickListener {
            onBackPressed()
        }

        btSubmit.setOnClickListener {
            submit()
        }

        profile = AppUtil.getProfile(this)
    }

    private fun initProgressDialog() {
        dialog = ProgressDialog(this);
        dialog.setMessage("Mohon tunggu..");
        dialog.setIndeterminate(true);
        dialog.setCanceledOnTouchOutside(false);
        dialog.setCancelable(false);
    }

    private fun submit() {
        val password = etPassword.text.toString()
        val confirmPassword = etConfirmPassword.text.toString()

        if (
            !TextUtils.isEmpty(password) &&
            !TextUtils.isEmpty(confirmPassword)
        ) {
            if (password != confirmPassword) {
                Toasty.info(this, "Password tidak sama", Toasty.LENGTH_LONG).show()
            } else {
                post()
            }
        } else {
            Toasty.info(this, "Mohon isi semua data", Toasty.LENGTH_LONG).show()
        }
    }

    private fun post() {
        val id = profile?.id ?: "0"
        val password = etPassword.text.toString()

        val disposable = ServerManager.getInstance()
            .service.changePassword(id, password)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .debounce(400, TimeUnit.MILLISECONDS)
            .doOnSubscribe { dialog.show() }
            .doOnTerminate { dialog.dismiss() }
            .subscribe(
                {
                    Toasty.success(this, "Ubah password berhasil", Toasty.LENGTH_LONG).show()
                    finish()
                },
                {
                    Toasty.error(this, "Ubah password gagal, silahkan coba lagi", Toasty.LENGTH_LONG).show()
                }
            )

        compositeDisposable.add(disposable)
    }

    override fun onDestroy() {
        compositeDisposable.clear()
        super.onDestroy()
    }
}
