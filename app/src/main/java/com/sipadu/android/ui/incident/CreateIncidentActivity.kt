package com.sipadu.android.ui.incident

import android.app.ProgressDialog
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.sipadu.android.R
import com.sipadu.android.model.Profile
import com.sipadu.android.server.ServerManager
import com.sipadu.android.ui.main.MainActivity
import com.sipadu.android.util.AppUtil
import com.sipadu.android.util.PermissionUtil
import es.dmoral.toasty.Toasty
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_create_incident.*
import java.util.concurrent.TimeUnit

class CreateIncidentActivity : AppCompatActivity() {

    lateinit var dialog: ProgressDialog

    private var kejadianId: String = ""
    private var name: String = ""

    private var latitude: Double = 0.0
    private var longitude: Double = 0.0

    var profile: Profile? = null
    private lateinit var fusedLocationClient: FusedLocationProviderClient

    private val compositeDisposable = CompositeDisposable()

    lateinit var alertDialog: AlertDialog

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_create_incident)

        intent.extras?.let {
            kejadianId = it.getString("kejadian_id", "0")
            name = it.getString("name", "")
        }

        initAlertDialog()

        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this)
        getCurrentLocation()

        profile = AppUtil.getProfile(this)

        tvName.text = name

        backIcon.setOnClickListener {
            onBackPressed()
        }

        initProgressDialog()

        btSubmit.setOnClickListener {
            submit()
        }
    }

    override fun onStart() {
        super.onStart()

        if (!PermissionUtil.isLocationPermissionGranted(this)) {
            alertDialog.show()
        }
    }

    private fun initProgressDialog() {
        dialog = ProgressDialog(this);
        dialog.setMessage("Mohon tunggu..");
        dialog.setIndeterminate(true);
        dialog.setCanceledOnTouchOutside(false);
        dialog.setCancelable(false);
    }

    private fun initAlertDialog() {
        alertDialog = AlertDialog.Builder(this)
            .setTitle("Location Permission")
            .setMessage("Sipadu Sidomulyo collects location data to enable creating incident report or laporan kejadian even when the app is closed or not in use.")
            .setCancelable(false)
            .setPositiveButton("Approve") { dialog, position ->
                alertDialog.dismiss()

                PermissionUtil.askLocationPermissions(this)
            }
            .setNegativeButton("Deny") { dialog, position ->
                alertDialog.dismiss()
                Toasty.info(this,
                    "Anda sudah menolak izin, silahkan berikan izin untuk melanjutkan",
                    Toasty.LENGTH_LONG).show()
                finish()
            }
            .create()
    }

    private fun getCurrentLocation() {
        // checking location permission
        if (!PermissionUtil.isLocationPermissionGranted(this)) return

        fusedLocationClient.lastLocation
            .addOnSuccessListener { location ->
                // getting the last known or current location
                latitude = location.latitude
                longitude = location.longitude
            }
            .addOnFailureListener {}
    }

    private fun submit() {
        val detail = etDetail.text.toString()
        val fullname = profile?.fullname ?: ""
        val userId = profile?.id ?: "0"
        val rt = profile?.rt ?: "0"
        val rw = profile?.rw ?: "0"

        val request = hashMapOf(
            "kejadian_id" to kejadianId,
            "nama_kejadian" to "${name.uppercase()}!!",
            "fullname" to fullname,
            "user_id" to userId,
            "rt" to rt,
            "rw" to rw,
            "lat" to latitude.toString(),
            "lng" to longitude.toString(),
            "detail" to detail
        )

        val disposable = ServerManager.getInstance()
            .service.createUserIncident(request)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .debounce(400, TimeUnit.MILLISECONDS)
            .doOnSubscribe { dialog.show() }
            .doOnTerminate { dialog.dismiss() }
            .subscribe(
                {
                    if (it.equals("Berhasil")) {
                        Toasty.success(this, "Buat laporan kejadian berhasil", Toasty.LENGTH_LONG).show()

                        startActivity(Intent(this, MainActivity::class.java).apply {
                            addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                            putExtra("selected_tab", "incident")
                        })

                    } else if (it.equals("Gagal Upload") || it.equals("No Image")) {
                        Toasty.error(this, "Upload gambar gagal, silahkan coba lagi", Toasty.LENGTH_LONG).show()
                    } else {
                        Toasty.error(this, "Buat laporan kejadian gagal, silahkan coba lagi", Toasty.LENGTH_LONG).show()
                    }
                },
                {
                    Toasty.error(this, "Buat laporan kejadian gagal, silahkan coba lagi", Toasty.LENGTH_LONG).show()
                }
            )

        compositeDisposable.add(disposable)
    }

    override fun onDestroy() {
        compositeDisposable.clear()
        super.onDestroy()
    }
}
