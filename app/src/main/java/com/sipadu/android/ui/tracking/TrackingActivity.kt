package com.sipadu.android.ui.tracking

import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import com.sipadu.android.R
import com.sipadu.android.adapter.TrackingLocationAdapter
import com.sipadu.android.model.Location
import com.sipadu.android.server.ServerManager
import com.sipadu.android.util.DateUtil
import io.github.luizgrp.sectionedrecyclerviewadapter.SectionedRecyclerViewAdapter
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_tracking.*
import java.util.concurrent.TimeUnit

class TrackingActivity : AppCompatActivity(), TrackingLocationAdapter.Listener {

    private val compositeDisposable = CompositeDisposable()

    private var userId: String = "0"
    private var fullname: String = ""
    private var position: String = ""
    private var nik: String = ""

    lateinit var adapter: SectionedRecyclerViewAdapter

    val itemList = HashMap<String, ArrayList<Location>>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_tracking)

        intent.extras?.let {
            userId = it.getString("user_id", "0")
            fullname = it.getString("fullname", "")
            position = it.getString("position", "")
            nik = it.getString("nik", "")
        }

        tvName.text = fullname
        tvPosition.text = position
        tvNik.text = "NIK. $nik"

        backIcon.setOnClickListener {
            onBackPressed()
        }

        adapter = SectionedRecyclerViewAdapter()

        recyclerView.adapter = adapter

        refreshLayout.setOnRefreshListener {
            loadData()
        }
    }

    override fun onResume() {
        super.onResume()
        loadData()
    }

    private fun loadData() {
        val disposable = ServerManager.getInstance()
            .service.loadTrackingLocation(userId)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .debounce(400, TimeUnit.MILLISECONDS)
            .doOnSubscribe { refreshLayout.isRefreshing = true }
            .doOnTerminate { refreshLayout.isRefreshing = false }
            .subscribe(
                {
                    adapter.removeAllSections()

                    var section = ""
                    val list = ArrayList<Location>()

                    for (i in 0..it.lastIndex) {
                        section = DateUtil.getDateWithShortMonthName(
                            context = this,
                            date = it[i].created_date,
                            inputFormat = DateUtil.MYSQL_DATE_TIME_FORMAT
                        )
                        list.add(it[i])

                        if (i < it.lastIndex) {
                            val nextSection = DateUtil.getDateWithShortMonthName(
                                context = this,
                                date = it[i + 1].created_date,
                                inputFormat = DateUtil.MYSQL_DATE_TIME_FORMAT
                            )

                            if (section != nextSection) {
                                val itemList = ArrayList<Location>()
                                itemList.addAll(list)
                                adapter.addSection(TrackingLocationAdapter(this, section, itemList).apply {
                                    listener = this@TrackingActivity
                                })
                                list.clear()
                            }
                        }
                    }

                    val itemList = ArrayList<Location>()
                    itemList.addAll(list)
                    adapter.addSection(TrackingLocationAdapter(this, section, itemList).apply {
                        listener = this@TrackingActivity
                    })
                    list.clear()

                    adapter.notifyDataSetChanged()
                },
                {
                    Log.d("Error", "Error getting datas.", it)
                }
            )

        compositeDisposable.add(disposable)
    }

    override fun onDestroy() {
        compositeDisposable.clear()
        super.onDestroy()
    }

    override fun onItemSelected(item: Location) {
        val intent = Intent(this, MapActivity::class.java)
        intent.putExtra("fullname", fullname)
        intent.putExtra("lat", item.lat)
        intent.putExtra("lng", item.lng)
        intent.putExtra("created_date", item.created_date)
        startActivity(intent)
    }
}
