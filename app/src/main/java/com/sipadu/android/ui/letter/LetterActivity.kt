package com.sipadu.android.ui.letter

import android.os.Bundle
import android.webkit.WebChromeClient
import android.webkit.WebResourceRequest
import android.webkit.WebView
import android.webkit.WebViewClient
import androidx.appcompat.app.AppCompatActivity
import com.sipadu.android.R
import io.reactivex.disposables.CompositeDisposable
import kotlinx.android.synthetic.main.activity_letter.*
import android.content.Intent
import android.net.Uri
import android.os.Message
import android.webkit.WebView.HitTestResult


class LetterActivity : AppCompatActivity() {

    private val compositeDisposable = CompositeDisposable()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_letter)

        backIcon.setOnClickListener {
            onBackPressed()
        }

        webView.apply {
            settings.apply {
                javaScriptEnabled = true
                javaScriptCanOpenWindowsAutomatically = true
                domStorageEnabled = true
                setSupportMultipleWindows(true)
            }

            webViewClient = object : WebViewClient() {
                override fun shouldOverrideUrlLoading(
                    view: WebView?,
                    request: WebResourceRequest?
                ): Boolean {
                    val url = request?.url.toString()
                    view?.loadUrl(url);

                    return super.shouldOverrideUrlLoading(view, request)
                }
            }
            webChromeClient = object : WebChromeClient() {
                override fun onCreateWindow(
                    view: WebView, dialog: Boolean, userGesture: Boolean, resultMsg: Message
                ): Boolean {
                    val result = view.hitTestResult
                    val data = result.extra
                    val context = view.context
                    val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse(data))
                    context.startActivity(browserIntent)
                    return false
                }
            }

            loadUrl("https://linktr.ee/Desa_Sidomulyo")
        }
    }

    override fun onDestroy() {
        compositeDisposable.clear()
        super.onDestroy()
    }
}
