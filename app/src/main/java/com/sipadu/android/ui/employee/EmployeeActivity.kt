package com.sipadu.android.ui.employee

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.sipadu.android.R
import com.sipadu.android.adapter.EmployeeAdapter
import com.sipadu.android.model.Profile
import com.sipadu.android.server.ServerManager
import com.sipadu.android.ui.tracking.TrackingActivity
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_employee.*
import java.util.concurrent.TimeUnit

class EmployeeActivity : AppCompatActivity(), EmployeeAdapter.Listener {

    private val compositeDisposable = CompositeDisposable()

    private var action: String = ""

    val itemList = ArrayList<Profile>()
    lateinit var adapter: EmployeeAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_employee)

        intent.extras?.let {
            action = it.getString("action", "")
        }

        if (action == "track_location") {
            tvHeaderTitle.text = "Track Lokasi Pegawai"
            iconAdd.visibility = View.GONE
        } else {
            tvHeaderTitle.text = "Pegawai"
            iconAdd.visibility = View.VISIBLE
        }

        backIcon.setOnClickListener {
            onBackPressed()
        }

        adapter = EmployeeAdapter()
        adapter.listener = this
        recyclerView.adapter = adapter

        refreshLayout.setOnRefreshListener {
            loadData()
        }

        iconAdd.setOnClickListener {
            startActivity(Intent(this, AddEmployeeActivity::class.java))
        }
    }

    override fun onResume() {
        super.onResume()
        loadData()
    }

    private fun loadData() {
        val disposable = ServerManager.getInstance()
            .service.loadEmployee()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .debounce(400, TimeUnit.MILLISECONDS)
            .doOnSubscribe { refreshLayout.isRefreshing = true }
            .doOnTerminate { refreshLayout.isRefreshing = false }
            .subscribe(
                {
                    itemList.clear()
                    itemList.addAll(it)
                    adapter.updateData(itemList)
                },
                {
                    Log.d("Error", "Error getting datas.", it)
                }
            )

        compositeDisposable.add(disposable)
    }

    override fun onDestroy() {
        compositeDisposable.clear()
        super.onDestroy()
    }

    override fun onItemSelected(item: Profile) {
        if (action == "track_location") {
            startActivity(Intent(this, TrackingActivity::class.java).apply {
                putExtra("user_id", item.id)
                putExtra("fullname", item.fullname)
                putExtra("position", item.position)
                putExtra("nik", item.nik)
            })
        } else {
            val intent = Intent(this, ViewEmployeeActivity::class.java)
            intent.putExtra("id", item.id)
            intent.putExtra("fullname", item.fullname)
            intent.putExtra("email", item.email)
            intent.putExtra("address", item.address)
            intent.putExtra("phone", item.phone)
            intent.putExtra("profile_pic", item.profile_pic)
            intent.putExtra("type", item.type)
            intent.putExtra("position", item.position)
            intent.putExtra("active", item.active)
            intent.putExtra("nik", item.nik)
            intent.putExtra("kk", item.no_kk)
            intent.putExtra("rt", item.rt)
            intent.putExtra("rw", item.rw)
            startActivity(intent)
        }
    }
}
