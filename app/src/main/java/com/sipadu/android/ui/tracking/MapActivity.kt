package com.sipadu.android.ui.tracking

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.sipadu.android.R
import com.sipadu.android.util.DateUtil
import kotlinx.android.synthetic.main.activity_map.*

class MapActivity : AppCompatActivity(), OnMapReadyCallback {

    private var fullname: String = ""
    private var lat: String = "0"
    private var lng: String = "0"
    private var createdDate: String = "0"

    private lateinit var mMap: GoogleMap

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_map)

        intent.extras?.let {
            fullname = it.getString("fullname", "")
            lat = it.getString("lat", "0")
            lng = it.getString("lng", "0")
            createdDate = it.getString("created_date", "")
        }

        backIcon.setOnClickListener {
            onBackPressed()
        }

        val mapFragment = supportFragmentManager
            .findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)
    }

    override fun onMapReady(googleMap: GoogleMap) {
        val time = DateUtil.convertDateString(
            date = createdDate,
            inputFormat = DateUtil.MYSQL_DATE_TIME_FORMAT,
            outputFormat = DateUtil.SIMPLE_TIME_FORMAT
        )
        val date = DateUtil.getDateWithShortMonthName(
            context = this,
            date = createdDate,
            inputFormat = DateUtil.MYSQL_DATE_TIME_FORMAT
        )

        mMap = googleMap

        val position = LatLng(lat.toDouble(), lng.toDouble())
        mMap.addMarker(
            MarkerOptions()
                .position(position)
                .title("Lokasi $fullname")
                .snippet("Pada $time, $date")
        )?.showInfoWindow()

        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(position, 16f))
    }
}
