package com.sipadu.android.ui.incident

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.sipadu.android.R
import com.sipadu.android.model.Profile
import com.sipadu.android.util.AppUtil
import com.sipadu.android.util.DateUtil
import kotlinx.android.synthetic.main.activity_incident_detail.*

class IncidentDetailActivity : AppCompatActivity(), OnMapReadyCallback {

    private var id: String = "0"
    private var kejadianId: String = "0"
    private var fullname: String = ""
    private var phone: String = ""
    private var name: String = ""
    private var lat: String = "0"
    private var lng: String = "0"
    private var detail: String = "0"
    private var rt: String = "0"
    private var rw: String = "0"
    private var createdDate: String = "0"

    private var dateTime: String = ""

    private lateinit var mMap: GoogleMap
    private var profile: Profile? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_incident_detail)

        intent.extras?.let {
            id = it.getString("id", "0")
            kejadianId = it.getString("kejadian_id", "0")
            fullname = it.getString("fullname", "")
            phone = it.getString("phone", "")
            name = it.getString("name", "")
            detail = it.getString("detail", "")
            rt = it.getString("rt", "")
            rw = it.getString("rw", "")
            lat = it.getString("lat", "0")
            lng = it.getString("lng", "0")
            createdDate = it.getString("created_date", "")
        }

        profile = AppUtil.getProfile(this)

        val time = DateUtil.convertDateString(
            date = createdDate,
            inputFormat = DateUtil.MYSQL_DATE_TIME_FORMAT,
            outputFormat = DateUtil.SIMPLE_TIME_FORMAT
        )
        val date = DateUtil.getDateWithShortMonthName(
            context = this,
            date = createdDate,
            inputFormat = DateUtil.MYSQL_DATE_TIME_FORMAT
        )
        dateTime = "$time, $date"

        backIcon.setOnClickListener {
            onBackPressed()
        }

        val mapFragment = supportFragmentManager
            .findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)

        initData()

        btCall.setOnClickListener {
            val intent = Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", phone, null))
            startActivity(intent)
        }

        btSendWa.setOnClickListener {
            val url = "https://api.whatsapp.com/send?phone=${phone}"
            val intent = Intent(Intent.ACTION_VIEW)
            intent.data = Uri.parse(url)
            startActivity(intent)
        }

        profile?.let {
            if (it.type == "admin" || it.type == "staff") {
                llActions.visibility = View.VISIBLE
            } else {
                llActions.visibility = View.GONE
            }
        }
    }

    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap

        val position = LatLng(lat.toDouble(), lng.toDouble())
        mMap.addMarker(
            MarkerOptions()
                .position(position)
                .title("$name")
                .snippet("Pada $dateTime")
        )?.showInfoWindow()

        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(position, 16f))
    }

    private fun initData() {
        tvName.text = fullname
        tvIncident.text = name
        tvDate.text = dateTime
        tvDetail.text = if (detail == "") "-" else detail
        tvRT.text = rt
        tvRW.text = rw
    }
}
