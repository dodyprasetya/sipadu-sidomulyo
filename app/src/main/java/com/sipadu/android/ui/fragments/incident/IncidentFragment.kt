package com.sipadu.android.ui.fragments.incident

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.sipadu.android.R
import com.sipadu.android.adapter.UserIncidentAdapter
import com.sipadu.android.model.UserIncident
import com.sipadu.android.server.ServerManager
import com.sipadu.android.ui.incident.IncidentActivity
import com.sipadu.android.ui.incident.IncidentDetailActivity
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.fragment_incident.*
import java.util.concurrent.TimeUnit

class IncidentFragment : Fragment(), UserIncidentAdapter.Listener {

    private val compositeDisposable = CompositeDisposable()

    val itemList = ArrayList<UserIncident>()
    lateinit var adapter: UserIncidentAdapter

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_incident, container, false)
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        adapter = UserIncidentAdapter()
        adapter.listener = this
        recyclerView.adapter = adapter

        refreshLayout.setOnRefreshListener {
            loadData()
        }

        iconAdd.setOnClickListener {
            startActivity(Intent(requireContext(), IncidentActivity::class.java).apply {
                putExtra("action", "select")
            })
        }
    }

    override fun onResume() {
        super.onResume()
        loadData()
    }

    private fun loadData() {
        val disposable = ServerManager.getInstance()
            .service.loadUserIncident()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .debounce(400, TimeUnit.MILLISECONDS)
            .doOnSubscribe { refreshLayout.isRefreshing = true }
            .doOnTerminate { refreshLayout.isRefreshing = false }
            .subscribe(
                {
                    itemList.clear()
                    itemList.addAll(it)
                    adapter.updateData(itemList)
                },
                {
                    Log.d("Error", "Error getting datas.", it)
                }
            )

        compositeDisposable.add(disposable)
    }

    override fun onDestroy() {
        compositeDisposable.clear()
        super.onDestroy()
    }

    override fun onItemSelected(item: UserIncident) {
        val intent = Intent(requireContext(), IncidentDetailActivity::class.java)
        intent.apply {
            with(item) {
                putExtra("id", id)
                putExtra("kejadian_id", kejadian_id)
                putExtra("user_id", user_id)
                putExtra("name", name)
                putExtra("fullname", fullname)
                putExtra("phone", phone)
                putExtra("lat", lat)
                putExtra("lng", lng)
                putExtra("detail", detail)
                putExtra("rt", rt)
                putExtra("rw", rw)
                putExtra("created_date", created_date)
            }
        }

        startActivity(intent)
    }
}