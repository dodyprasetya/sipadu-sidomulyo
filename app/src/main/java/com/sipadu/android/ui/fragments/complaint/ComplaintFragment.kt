package com.sipadu.android.ui.fragments.complaint

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.sipadu.android.R
import com.sipadu.android.adapter.ComplaintAdapter
import com.sipadu.android.model.Complaint
import com.sipadu.android.model.Profile
import com.sipadu.android.server.ServerManager
import com.sipadu.android.ui.complaint.ComplaintDetailActivity
import com.sipadu.android.ui.complaint.CreateComplaintActivity
import com.sipadu.android.util.AppUtil
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.fragment_complaint.*
import java.util.concurrent.TimeUnit

class ComplaintFragment : Fragment(), ComplaintAdapter.Listener {

    private val compositeDisposable = CompositeDisposable()

    val itemList = ArrayList<Complaint>()
    lateinit var adapter: ComplaintAdapter
    var profile: Profile? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_complaint, container, false)
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        profile = AppUtil.getProfile(requireContext())
        profile?.let {
            if (it.type.equals("staff") || it.type.equals("admin")) {
                tvHeaderTitle.text = "Pengaduan Warga"
            }
        }

        adapter = ComplaintAdapter(profile?.id ?: "0")
        adapter.listener = this
        recyclerView.adapter = adapter

        refreshLayout.setOnRefreshListener {
            loadData()
        }

        iconAdd.setOnClickListener {
            startActivity(Intent(requireContext(), CreateComplaintActivity::class.java))
        }
    }

    override fun onResume() {
        super.onResume()
        loadData()
    }

    private fun loadData() {
        val observable: Observable<List<Complaint>>

        if (profile?.type == "user") {
            observable = ServerManager.getInstance().service.loadComplaints(profile?.id ?: "0")
        } else {
            observable = ServerManager.getInstance().service.loadAllComplaints()
        }

        val disposable = observable
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .debounce(400, TimeUnit.MILLISECONDS)
            .doOnSubscribe { refreshLayout.isRefreshing = true }
            .doOnTerminate { refreshLayout.isRefreshing = false }
            .subscribe(
                {
                    itemList.clear()
                    itemList.addAll(it)
                    adapter.updateData(itemList)
                },
                {
                    Log.d("Error", "Error getting datas.", it)
                }
            )

        compositeDisposable.add(disposable)
    }

    override fun onDestroy() {
        compositeDisposable.clear()
        super.onDestroy()
    }

    override fun onItemSelected(item: Complaint) {
        val intent = Intent(requireContext(), ComplaintDetailActivity::class.java)

        intent.apply {
            with(item) {
                putExtra("id", id)
                putExtra("title", title)
                putExtra("content", content)
                putExtra("photo", photo)
                putExtra("user_id", user_id)
                putExtra("fullname", fullname)
                putExtra("created_date", created_date)
            }
        }

        startActivity(intent)
    }
}