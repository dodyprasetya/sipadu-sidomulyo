package com.sipadu.android.ui.employee

import android.app.ProgressDialog
import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.sipadu.android.R
import com.sipadu.android.server.ServerManager
import com.sipadu.android.ui.profile.PasswordChangedActivity
import es.dmoral.toasty.Toasty
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_view_employee.*
import java.util.concurrent.TimeUnit

class ViewEmployeeActivity : AppCompatActivity() {

    private val compositeDisposable = CompositeDisposable()

    lateinit var dialog: ProgressDialog
    lateinit var alertDialog: AlertDialog
    lateinit var deleteAlertDialog: AlertDialog

    private var id: String = "0"
    private var fullname: String = ""
    private var email: String = ""
    private var phone: String = ""
    private var address: String = ""
    private var profilePic: String = ""
    private var type: String = ""
    private var position: String = ""
    private var active: String = ""
    private var nik: String = ""
    private var kk: String = ""
    private var rt: String = ""
    private var rw: String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_view_employee)

        intent.extras?.let {
            id = it.getString("id", "0")
            fullname = it.getString("fullname", "")
            email = it.getString("email", "")
            phone = it.getString("phone", "")
            address = it.getString("address", "")
            profilePic = it.getString("profile_pic", "")
            type = it.getString("type", "")
            position = it.getString("position", "")
            active = it.getString("active", "")
            nik = it.getString("nik", "")
            kk = it.getString("kk", "")
            rt = it.getString("rt", "")
            rw = it.getString("rw", "")

            initData()
        }

        initAlertDialog()
        initDeleteAlertDialog()

        backIcon.setOnClickListener {
            onBackPressed()
        }

        initProgressDialog()

        btActivate.setOnClickListener {
            activateAccount()
        }

        btNonActivate.setOnClickListener {
            nonActivateAccount()
        }

        btResetPassword.setOnClickListener {
            alertDialog.show()
        }

        btDelete.setOnClickListener {
            deleteAlertDialog.show()
        }
    }

    private fun initProgressDialog() {
        dialog = ProgressDialog(this);
        dialog.setMessage("Mohon tunggu..");
        dialog.setIndeterminate(true);
        dialog.setCanceledOnTouchOutside(false);
        dialog.setCancelable(false);
    }

    private fun initAlertDialog() {
        alertDialog = AlertDialog.Builder(this)
            .setTitle("Reset Password")
            .setMessage("Anda yakin ingin mengatur ulang password staf ini?")
            .setCancelable(false)
            .setPositiveButton("Ya") { dialog, position ->
                alertDialog.dismiss()
                resetPassword()
            }
            .setNegativeButton("Tidak") { dialog, position ->
                alertDialog.dismiss()
            }
            .create()
    }

    private fun initDeleteAlertDialog() {
        deleteAlertDialog = AlertDialog.Builder(this)
            .setTitle("Hapus Akun Pegawai")
            .setMessage("Anda yakin ingin menghapus akun pegawai ini secara permanen? Akun yang telah dihapus tidak bisa dikembalikan lagi.")
            .setCancelable(false)
            .setPositiveButton("Ya") { dialog, position ->
                deleteAlertDialog.dismiss()
                deleteUser()
            }
            .setNegativeButton("Tidak") { dialog, position ->
                deleteAlertDialog.dismiss()
            }
            .create()
    }

    private fun initData() {
        tvName.text = fullname
        tvPhone.text = phone
        tvEmail.text = email
        tvAddress.text = address
        tvNik.text = nik
        tvPosition.text = position
        tvKK.text = kk
        tvRT.text = rt
        tvRW.text = rw

        val isAccountActive = active == "1"
        tvStatus.text = if (!isAccountActive) "Belum Aktif" else "Aktif"
        tvStatus.setTextColor(
            if (isAccountActive) ContextCompat.getColor(this, R.color.primary)
            else ContextCompat.getColor(this, R.color.red)
        )

        btActivate.visibility = if (!isAccountActive) View.VISIBLE else View.GONE
        btResetPassword.visibility = if (isAccountActive) View.VISIBLE else View.GONE
        btNonActivate.visibility = if (isAccountActive) View.VISIBLE else View.GONE

        Glide.with(this)
            .load(profilePic)
            .diskCacheStrategy(DiskCacheStrategy.ALL)
            .circleCrop()
            .into(ivProfile)
    }

    private fun activateAccount() {
        val disposable = ServerManager.getInstance()
            .service.activateUser(id, "1")
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .debounce(400, TimeUnit.MILLISECONDS)
            .doOnSubscribe { dialog.show() }
            .doOnTerminate { dialog.dismiss() }
            .subscribe(
                {
                    Toasty.success(this, "Akun berhasil diaktifkan", Toasty.LENGTH_LONG).show()
                    active = "1"
                    initData()
                },
                {
                    Toasty.error(this, "Oops, gagal mengubah data", Toasty.LENGTH_LONG).show()
                }
            )

        compositeDisposable.add(disposable)
    }

    private fun nonActivateAccount() {
        val disposable = ServerManager.getInstance()
            .service.activateUser(id, "0")
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .debounce(400, TimeUnit.MILLISECONDS)
            .doOnSubscribe { dialog.show() }
            .doOnTerminate { dialog.dismiss() }
            .subscribe(
                {
                    Toasty.success(this, "Akun berhasil dinonaktifkan", Toasty.LENGTH_LONG).show()
                    active = "0"
                    initData()
                },
                {
                    Toasty.error(this, "Oops, gagal mengubah data", Toasty.LENGTH_LONG).show()
                }
            )

        compositeDisposable.add(disposable)
    }

    private fun resetPassword() {
        val randomNumber = (10000..99999).random().toString()

        val disposable = ServerManager.getInstance()
            .service.changePassword(id, randomNumber)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .debounce(400, TimeUnit.MILLISECONDS)
            .doOnSubscribe { dialog.show() }
            .doOnTerminate { dialog.dismiss() }
            .subscribe(
                {
                    Toasty.success(this, "Berhasil reset password", Toasty.LENGTH_LONG).show()

                    startActivity(Intent(this, PasswordChangedActivity::class.java).apply {
                        putExtra("password", randomNumber)
                    })
                },
                {
                    Toasty.error(this, "Oops, gagal reset password", Toasty.LENGTH_LONG).show()
                }
            )

        compositeDisposable.add(disposable)
    }

    private fun deleteUser() {
        val disposable = ServerManager.getInstance()
            .service.deleteUser(id)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .debounce(400, TimeUnit.MILLISECONDS)
            .doOnSubscribe { dialog.show() }
            .doOnTerminate { dialog.dismiss() }
            .subscribe(
                {
                    Toasty.success(this, "Berhasil hapus akun", Toasty.LENGTH_LONG).show()
                    finish()
                },
                {
                    Toasty.error(this, "Oops, gagal hapus akun", Toasty.LENGTH_LONG).show()
                }
            )

        compositeDisposable.add(disposable)
    }

    override fun onDestroy() {
        compositeDisposable.clear()
        super.onDestroy()
    }
}