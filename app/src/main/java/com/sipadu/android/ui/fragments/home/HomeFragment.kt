package com.sipadu.android.ui.fragments.home

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.sipadu.android.R
import com.sipadu.android.model.Profile
import com.sipadu.android.ui.complaint.CreateComplaintActivity
import com.sipadu.android.ui.employee.EmployeeActivity
import com.sipadu.android.ui.incident.IncidentActivity
import com.sipadu.android.ui.info.InfoActivity
import com.sipadu.android.ui.letter.LetterActivity
import com.sipadu.android.ui.report.ReportActivity
import com.sipadu.android.ui.user.RegisteredUserActivity
import com.sipadu.android.ui.user.UserActivity
import com.sipadu.android.ui.website.WebsiteActivity
import com.sipadu.android.util.AppUtil
import io.reactivex.disposables.CompositeDisposable
import kotlinx.android.synthetic.main.fragment_home.*

class HomeFragment : Fragment() {

    private val compositeDisposable = CompositeDisposable()

    var profile: Profile? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_home, container, false)
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        profile = AppUtil.getProfile(requireContext())
        profile?.let {
            Glide.with(requireContext())
                .load(it.profile_pic)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .circleCrop()
                .into(ivProfile)

            tvName.text = it.fullname
            tvNik.text = "NIK. ${it.nik}"
            tvPosition.text = it.position

            hideShowMenus(it)
        }

        buttonComplaint.setOnClickListener {
            startActivity(Intent(context, CreateComplaintActivity::class.java))
        }

        buttonIncident.setOnClickListener {
            startActivity(Intent(requireContext(), IncidentActivity::class.java).apply {
                putExtra("action", "select")
            })
        }

        buttonInfo.setOnClickListener {
            startActivity(Intent(context, InfoActivity::class.java))
        }

        buttonLetter.setOnClickListener {
            startActivity(Intent(context, LetterActivity::class.java))
        }

        buttonWebsite.setOnClickListener {
            startActivity(Intent(context, WebsiteActivity::class.java))
        }

        buttonTracking.setOnClickListener {
            startActivity(Intent(context, EmployeeActivity::class.java).apply {
                putExtra("action", "track_location")
            })
        }

        llRegisteredUser.setOnClickListener {
            startActivity(Intent(context, RegisteredUserActivity::class.java))
        }

        llAllUsers.setOnClickListener {
            startActivity(Intent(context, UserActivity::class.java))
        }

        llEmployee.setOnClickListener {
            startActivity(Intent(context, EmployeeActivity::class.java))
        }

        llSettingIncident.setOnClickListener {
            startActivity(Intent(context, IncidentActivity::class.java).apply {
                putExtra("action", "manage")
            })
        }

        llIncidentReport.setOnClickListener {
            startActivity(Intent(context, ReportActivity::class.java))
        }
    }

    private fun hideShowMenus(profile: Profile) {
        if (profile.type == "admin") {
            buttonTracking.visibility = View.VISIBLE
        } else {
            llManage.visibility = View.GONE
            buttonTracking.visibility = View.INVISIBLE
        }

        if (profile.type == "admin") {
            llManage.visibility = View.VISIBLE

            llRegisteredUser.visibility = View.VISIBLE
            vRegisteredUser.visibility = View.VISIBLE
            llAllUsers.visibility = View.VISIBLE
            vAllUsers.visibility = View.VISIBLE
            llEmployee.visibility = View.VISIBLE
            vEmployee.visibility = View.VISIBLE
            llSettingIncident.visibility = View.VISIBLE

            llIncidentReport.visibility = View.VISIBLE
            vIncidentReport.visibility = View.VISIBLE
            tvIncidentReport.text = "Laporan Kegiatan Pegawai"

        } else if (profile.type == "staff") {
            llManage.visibility = View.VISIBLE

            llRegisteredUser.visibility = View.GONE
            vRegisteredUser.visibility = View.GONE
            llAllUsers.visibility = View.GONE
            vAllUsers.visibility = View.GONE
            llEmployee.visibility = View.GONE
            vEmployee.visibility = View.GONE
            llSettingIncident.visibility = View.GONE

            llIncidentReport.visibility = View.VISIBLE
            vIncidentReport.visibility = View.VISIBLE
            tvIncidentReport.text = "Laporan Kegiatan Anda"
        }
    }

    override fun onDestroy() {
        compositeDisposable.clear()
        super.onDestroy()
    }
}