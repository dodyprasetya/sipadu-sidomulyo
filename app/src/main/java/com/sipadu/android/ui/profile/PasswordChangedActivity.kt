package com.sipadu.android.ui.profile

import android.content.ClipData
import android.content.ClipboardManager
import android.content.Context
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.sipadu.android.R
import es.dmoral.toasty.Toasty
import kotlinx.android.synthetic.main.activity_password_changed.*

class PasswordChangedActivity : AppCompatActivity() {

    private var password: String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_password_changed)

        intent.extras?.let {
            password = it.getString("password", "")

            tvPassword.text = password
        }

        backIcon.setOnClickListener {
            onBackPressed()
        }
    }

    fun copyPassword(view: View) {
        val clipboardManager = getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager
        val clipData = ClipData.newPlainText("text", password)
        clipboardManager.setPrimaryClip(clipData)

        Toasty.info(this, "Password disalin", Toasty.LENGTH_SHORT).show()
    }
}