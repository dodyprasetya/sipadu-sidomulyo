package com.sipadu.android.ui.incident

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.sipadu.android.R
import com.sipadu.android.adapter.IncidentAdapter
import com.sipadu.android.model.Incident
import com.sipadu.android.server.ServerManager
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_incident.*
import java.util.concurrent.TimeUnit

class IncidentActivity : AppCompatActivity(), IncidentAdapter.Listener {

    private val compositeDisposable = CompositeDisposable()

    private var action: String = ""

    val itemList = ArrayList<Incident>()
    lateinit var adapter: IncidentAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_incident)

        intent.extras?.let {
            action = it.getString("action", "select")
        }

        if (action == "select") {
            iconAdd.visibility = View.GONE
            tvHeaderTitle.text = "Pilih Kejadian"
        } else if (action == "manage") {
            iconAdd.visibility = View.VISIBLE
            tvHeaderTitle.text = "Daftar Kejadian"
        }

        backIcon.setOnClickListener {
            onBackPressed()
        }

        adapter = IncidentAdapter()
        adapter.listener = this
        recyclerView.adapter = adapter

        refreshLayout.setOnRefreshListener {
            loadData()
        }

        iconAdd.setOnClickListener {
            startActivity(Intent(this, AddIncidentActivity::class.java))
        }
    }

    override fun onResume() {
        super.onResume()
        loadData()
    }

    private fun loadData() {
        val disposable = ServerManager.getInstance()
            .service.loadIncidentTypes()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .debounce(400, TimeUnit.MILLISECONDS)
            .doOnSubscribe { refreshLayout.isRefreshing = true }
            .doOnTerminate { refreshLayout.isRefreshing = false }
            .subscribe(
                {
                    itemList.clear()
                    itemList.addAll(it)
                    adapter.updateData(itemList)
                },
                {
                    Log.d("Error", "Error getting datas.", it)
                }
            )

        compositeDisposable.add(disposable)
    }

    override fun onDestroy() {
        compositeDisposable.clear()
        super.onDestroy()
    }

    override fun onItemSelected(item: Incident) {
        if (action == "select") {
            startActivity(Intent(this, CreateIncidentActivity::class.java).apply {
                putExtra("kejadian_id", item.id)
                putExtra("name", item.name)
            })
        }
    }
}
