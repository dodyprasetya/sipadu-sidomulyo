package com.sipadu.android.ui.incident

import android.app.ProgressDialog
import android.os.Bundle
import android.text.TextUtils
import androidx.appcompat.app.AppCompatActivity
import com.sipadu.android.R
import com.sipadu.android.server.ServerManager
import com.sipadu.android.util.RequestUtil
import es.dmoral.toasty.Toasty
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_add_incident.*
import java.util.concurrent.TimeUnit

class AddIncidentActivity : AppCompatActivity() {

    lateinit var dialog: ProgressDialog

    private val compositeDisposable = CompositeDisposable()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_incident)

        backIcon.setOnClickListener {
            onBackPressed()
        }

        initProgressDialog()

        btSubmit.setOnClickListener {
            submit()
        }
    }

    private fun initProgressDialog() {
        dialog = ProgressDialog(this);
        dialog.setMessage("Mohon tunggu..");
        dialog.setIndeterminate(true);
        dialog.setCanceledOnTouchOutside(false);
        dialog.setCancelable(false);
    }

    private fun submit() {
        val name = etTitle.text.toString()

        if (
            !TextUtils.isEmpty(name)
        ) {
            val request = hashMapOf(
                "name" to RequestUtil.getBody(name)
            )

            val disposable = ServerManager.getInstance()
                .service.createIncident(request)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .debounce(400, TimeUnit.MILLISECONDS)
                .doOnSubscribe { dialog.show() }
                .doOnTerminate { dialog.dismiss() }
                .subscribe(
                    {
                        if (it.equals("Berhasil")) {
                            Toasty.success(this, "Tambah jenis kejadian berhasil", Toasty.LENGTH_LONG).show()
                            finish()
                        } else if (it.equals("Gagal Upload") || it.equals("No Image")) {
                            Toasty.error(this, "Upload gambar gagal, silahkan coba lagi", Toasty.LENGTH_LONG).show()
                        } else {
                            Toasty.error(this, "Tambah jenis kejadian gagal, silahkan coba lagi", Toasty.LENGTH_LONG).show()
                        }
                    },
                    {
                        Toasty.error(this, "Tambah jenis kejadian gagal, silahkan coba lagi", Toasty.LENGTH_LONG).show()
                    }
                )

            compositeDisposable.add(disposable)

        } else {
            Toasty.info(this, "Mohon isi semua data", Toasty.LENGTH_LONG).show()
        }
    }

    override fun onDestroy() {
        compositeDisposable.clear()
        super.onDestroy()
    }
}
