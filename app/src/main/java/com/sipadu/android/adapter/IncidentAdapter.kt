package com.sipadu.android.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.sipadu.android.R
import com.sipadu.android.model.Incident
import kotlinx.android.synthetic.main.item_incident.view.*

class IncidentAdapter : RecyclerView.Adapter<IncidentAdapter.ViewHolder>(){

    private lateinit var itemList: List<Incident>
    private var context: Context? = null
    var listener: Listener? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        context = parent.context
        val view = LayoutInflater.from(context).inflate(R.layout.item_incident, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return if (::itemList.isInitialized) itemList.size else 0
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind()
    }

    fun updateData(list: ArrayList<Incident>) {
        itemList = list;
        notifyDataSetChanged()
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bind() {
            val item = itemList.get(adapterPosition)

            itemView.tvIncident.text = item.name

            itemView.container.setOnClickListener {
                listener?.onItemSelected(item)
            }
        }
    }

    interface Listener {
        fun onItemSelected(item: Incident)
    }
}