package com.sipadu.android.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.sipadu.android.R
import com.sipadu.android.model.Profile
import com.sipadu.android.util.DateUtil
import kotlinx.android.synthetic.main.item_user.view.*

class UserAdapter : RecyclerView.Adapter<UserAdapter.ViewHolder>(){

    private lateinit var itemList: List<Profile>
    private var context: Context? = null
    var listener: Listener? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        context = parent.context
        val view = LayoutInflater.from(context).inflate(R.layout.item_user, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return if (::itemList.isInitialized) itemList.size else 0
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind()
    }

    fun updateData(list: ArrayList<Profile>) {
        itemList = list;
        notifyDataSetChanged()
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bind() {
            val item = itemList.get(adapterPosition)

            itemView.tvName.text = item.fullname
            itemView.tvPhone.text = item.phone
            itemView.tvDate.text = DateUtil.getDateWithShortMonthName(
                context!!, item.created_date, DateUtil.MYSQL_DATE_TIME_FORMAT
            )

            Glide.with(context!!)
                .load(item.profile_pic)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .circleCrop()
                .into(itemView.ivProfile)

            itemView.container.setOnClickListener {
                listener?.onItemSelected(item)
            }
        }
    }

    interface Listener {
        fun onItemSelected(item: Profile)
    }
}