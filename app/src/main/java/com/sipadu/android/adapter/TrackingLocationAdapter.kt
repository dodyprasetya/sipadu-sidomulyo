package com.sipadu.android.adapter

import android.content.Context
import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.sipadu.android.R
import com.sipadu.android.model.Location
import com.sipadu.android.util.DateUtil
import io.github.luizgrp.sectionedrecyclerviewadapter.Section
import io.github.luizgrp.sectionedrecyclerviewadapter.SectionParameters
import kotlinx.android.synthetic.main.item_location.view.*
import kotlinx.android.synthetic.main.item_location.view.container
import kotlinx.android.synthetic.main.item_location_section.view.*

class TrackingLocationAdapter(
    val ctx: Context, val sectionTitle: String, val itemList: List<Location>
) : Section(
    SectionParameters.builder()
        .itemResourceId(R.layout.item_location)
        .headerResourceId(R.layout.item_location_section)
        .build()
) {

    var listener: Listener? = null

    override fun getContentItemsTotal(): Int {
        return itemList.size
    }

    override fun getItemViewHolder(view: View): RecyclerView.ViewHolder {
        return ItemViewHolder(view)
    }

    override fun onBindItemViewHolder(holder: RecyclerView.ViewHolder?, position: Int) {
        val viewHolder = holder as ItemViewHolder
        viewHolder.bind(position)
    }

    override fun getHeaderViewHolder(view: View): RecyclerView.ViewHolder {
        return HeaderViewHolder(view)
    }

    override fun onBindHeaderViewHolder(holder: RecyclerView.ViewHolder?) {
        val viewHolder = holder as HeaderViewHolder
        viewHolder.bind()
    }

    inner class HeaderViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bind() {
            itemView.tvSectionTitle.text = sectionTitle
        }
    }

    inner class ItemViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bind(position: Int) {
            val item = itemList.get(position)

            item.apply {
                val time = DateUtil.convertDateString(
                    date = created_date,
                    inputFormat = DateUtil.MYSQL_DATE_TIME_FORMAT,
                    outputFormat = DateUtil.SIMPLE_TIME_FORMAT
                )
                val date = DateUtil.getDateWithShortMonthName(
                    context = ctx,
                    date = created_date,
                    inputFormat = DateUtil.MYSQL_DATE_TIME_FORMAT
                )
                itemView.tvDate.text = "Pukul ${time}"
            }

            itemView.container.setOnClickListener {
                listener?.onItemSelected(item)
            }

            with(itemView) {
                if (itemList.size == 1) {
                    vTopLine.visibility = View.GONE
                    vCircle.visibility = View.VISIBLE
                    vBottomLine.visibility = View.GONE
                } else {
                    if (position == 0) {
                        vTopLine.visibility = View.GONE
                        vCircle.visibility = View.VISIBLE
                        vBottomLine.visibility = View.VISIBLE
                    } else if (position == itemList.lastIndex) {
                        vTopLine.visibility = View.VISIBLE
                        vCircle.visibility = View.VISIBLE
                        vBottomLine.visibility = View.GONE
                    } else {
                        vTopLine.visibility = View.VISIBLE
                        vCircle.visibility = View.GONE
                        vBottomLine.visibility = View.VISIBLE
                    }
                }
            }
        }
    }

    interface Listener {
        fun onItemSelected(item: Location)
    }
}