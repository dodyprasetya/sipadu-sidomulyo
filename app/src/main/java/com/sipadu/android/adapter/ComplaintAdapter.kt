package com.sipadu.android.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.sipadu.android.R
import com.sipadu.android.model.Complaint
import com.sipadu.android.util.DateUtil
import kotlinx.android.synthetic.main.item_complaint.view.*
import kotlinx.android.synthetic.main.item_complaint.view.container
import kotlinx.android.synthetic.main.item_complaint.view.tvDate
import kotlinx.android.synthetic.main.item_complaint.view.tvName

class ComplaintAdapter(val userId: String) : RecyclerView.Adapter<ComplaintAdapter.ViewHolder>() {

    private lateinit var itemList: List<Complaint>
    private var context: Context? = null
    var listener: Listener? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        context = parent.context
        val view = LayoutInflater.from(context).inflate(R.layout.item_complaint, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return if (::itemList.isInitialized) itemList.size else 0
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind()
    }

    fun updateData(list: ArrayList<Complaint>) {
        itemList = list;
        notifyDataSetChanged()
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bind() {
            val item = itemList.get(adapterPosition)

            item.apply {
                if (photo != "") {
                    itemView.ivPhoto.visibility = View.VISIBLE
                    itemView.ivComplaint.visibility = View.GONE

                    Glide.with(context!!)
                        .load(photo)
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .centerCrop()
                        .into(itemView.ivPhoto)
                } else {
                    itemView.ivPhoto.visibility = View.GONE
                    itemView.ivComplaint.visibility = View.VISIBLE
                }

                itemView.tvTitle.text = title

                val time = DateUtil.convertDateString(
                    date = created_date,
                    inputFormat = DateUtil.MYSQL_DATE_TIME_FORMAT,
                    outputFormat = DateUtil.SIMPLE_TIME_FORMAT
                )
                val date = DateUtil.getDateWithShortMonthName(
                    context = context!!,
                    date = created_date,
                    inputFormat = DateUtil.MYSQL_DATE_TIME_FORMAT
                )
                itemView.tvDate.text = "${time}, ${date}"

                if (user_id == userId) {
                    itemView.llFullname.visibility = View.GONE
                } else {
                    itemView.llFullname.visibility = View.VISIBLE
                    itemView.tvName.text = fullname
                }
            }

            itemView.container.setOnClickListener {
                listener?.onItemSelected(item)
            }
        }
    }

    interface Listener {
        fun onItemSelected(item: Complaint)
    }
}