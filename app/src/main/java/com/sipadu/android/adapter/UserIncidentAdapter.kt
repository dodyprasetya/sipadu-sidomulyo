package com.sipadu.android.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.Nullable
import androidx.recyclerview.widget.RecyclerView
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.MapsInitializer
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.sipadu.android.R
import com.sipadu.android.model.UserIncident
import com.sipadu.android.util.DateUtil
import kotlinx.android.synthetic.main.item_incident_user.view.*

class UserIncidentAdapter : RecyclerView.Adapter<UserIncidentAdapter.ViewHolder>(){

    private lateinit var itemList: List<UserIncident>
    private var context: Context? = null
    var listener: Listener? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        context = parent.context
        val view = LayoutInflater.from(context).inflate(R.layout.item_incident_user, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return if (::itemList.isInitialized) itemList.size else 0
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind()
    }

    fun updateData(list: ArrayList<UserIncident>) {
        itemList = list;
        notifyDataSetChanged()
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView), OnMapReadyCallback {

        private lateinit var map: GoogleMap
        private lateinit var latLng: LatLng

        init {
            with(itemView.mapView) {
                onCreate(null)
                getMapAsync(this@ViewHolder)
            }
        }

        fun bind() {
            val item = itemList.get(adapterPosition)

            itemView.tvIncident.text = item.name
            itemView.tvName.text = item.fullname

            val time = DateUtil.convertDateString(
                date = item.created_date,
                inputFormat = DateUtil.MYSQL_DATE_TIME_FORMAT,
                outputFormat = DateUtil.SIMPLE_TIME_FORMAT
            )
            val date = DateUtil.getDateWithShortMonthName(
                context = context!!,
                date = item.created_date,
                inputFormat = DateUtil.MYSQL_DATE_TIME_FORMAT
            )
            itemView.tvDate.text = "${time}, ${date}"

            itemView.mapView.tag = this
            latLng = LatLng(item.lat.toDouble(), item.lng.toDouble())
            setMapLocation(item)

            itemView.container.setOnClickListener {
                listener?.onItemSelected(item)
            }
        }

        override fun onMapReady(googleMap: GoogleMap) {
            MapsInitializer.initialize(context!!)

            // If map is not initialised properly
            map = googleMap ?: return
            setMapLocation(null)
        }

        private fun setMapLocation(@Nullable item: UserIncident?) {
            if (!::map.isInitialized) return

            with(map) {
                moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 16f))
                addMarker(MarkerOptions().position(latLng))
                mapType = GoogleMap.MAP_TYPE_NORMAL
                uiSettings.apply {
                    setAllGesturesEnabled(false)
                }
                setOnMapClickListener {
                    item?.let { listener?.onItemSelected(it) }
                }
            }
        }
    }

    interface Listener {
        fun onItemSelected(item: UserIncident)
    }
}