package com.sipadu.android.firebase

import android.app.PendingIntent
import android.content.Intent
import android.util.Log
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import com.sipadu.android.R
import com.sipadu.android.app.Constants
import com.sipadu.android.server.ServerManager
import com.sipadu.android.ui.info.InfoActivity
import com.sipadu.android.ui.login.LoginActivity
import com.sipadu.android.ui.main.MainActivity
import com.sipadu.android.ui.report.ReportActivity
import com.sipadu.android.util.AppUtil
import com.sipadu.android.util.NotificationUtil
import com.sipadu.android.util.PrefUtil
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import java.util.concurrent.TimeUnit

class MessagingService : FirebaseMessagingService() {

    private val TAG = "FirebaseService"

    private val TYPE_ACCOUNT = "ACCOUNT";
    private val TYPE_COMPLAINT = "COMPLAINT";
    private val TYPE_INCIDENT = "INCIDENT";
    private val TYPE_REPORT = "REPORT";

    private val TYPE = "type";
    private val TITLE = "title";
    private val MESSAGE = "message";
    private val PRIORITY = "priority";
    private val ID = "id";

    private val compositeDisposable = CompositeDisposable()

    override fun onMessageReceived(remoteMessage: RemoteMessage) {
        Log.d(TAG, "From: " + remoteMessage.from)

        if (remoteMessage.notification != null)
            Log.d(TAG, "onMessageReceived: " + remoteMessage.notification!!.body)

        // Check if message contains a data payload.
        if (remoteMessage.data.isNotEmpty()) {
            Log.d(TAG, "Message data payload: " + remoteMessage.data)

            // process notification
            remoteMessage.from?.let { from ->
                processNotification(remoteMessage.data, from)
            }
        }
    }

    override fun onNewToken(token: String) {
        super.onNewToken(token)

        // save token
        PrefUtil.write(applicationContext, Constants.PREF_FCM_TOKEN, token)

        // send to server if already login
        if (AppUtil.isLogin(this)) {
            val profile = AppUtil.getProfile(this)!!

            val disposable = ServerManager.getInstance()
                .service.updateFcmToken(profile.id, token)
                .subscribeOn(Schedulers.io())
                .observeOn(Schedulers.io())
                .debounce(400, TimeUnit.MILLISECONDS)
                .subscribe({}, {})

            compositeDisposable.add(disposable)
        }
    }

    fun processNotification(data: Map<String, String>, from: String) {
        val isLogin = AppUtil.isLogin(this)

        val intent: Intent;

        if (from.equals("/topics/" + Constants.FIREBASE_GLOBAL_TOPIC)) {
            intent = Intent(this, LoginActivity::class.java)
        } else if (from.equals("/topics/" + Constants.FIREBASE_INFO_TOPIC)) {
            if (isLogin) {
                intent = Intent(this, InfoActivity::class.java).apply {
                    putExtra("id", data[ID])
                }
            } else {
                intent = Intent(this, LoginActivity::class.java)
            }
        } else {
            if (data[TYPE].equals(TYPE_COMPLAINT)) {
                if (isLogin) {
                    intent = Intent(this, MainActivity::class.java).apply {
                        putExtra("selected_tab", "complaint")
                        putExtra("complaint_id", data[ID])
                    }
                } else {
                    intent = Intent(this, LoginActivity::class.java)
                }
            }
            else if (data[TYPE].equals(TYPE_INCIDENT)) {
                if (isLogin) {
                    intent = Intent(this, MainActivity::class.java).apply {
                        putExtra("selected_tab", "incident")
                        putExtra("incident_id", data[ID])
                    }
                } else {
                    intent = Intent(this, LoginActivity::class.java)
                }
            }
            else if (data[TYPE].equals(TYPE_REPORT)) {
                if (isLogin) {
                    intent = Intent(this, ReportActivity::class.java)
                } else {
                    intent = Intent(this, LoginActivity::class.java)
                }
            }
            else {
                intent = Intent(this, LoginActivity::class.java)
            }
        }

        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)

        val pendingIntent = PendingIntent.getActivity(this,
            NotificationUtil.NOTIFICATION_REQUEST_CODE, intent,
            PendingIntent.FLAG_ONE_SHOT)

        val title = data[TITLE] ?: resources.getString(R.string.app_name)
        val message = data[MESSAGE] ?: ""
        val priority = data[PRIORITY] ?: ""

        NotificationUtil.showNotification(this, pendingIntent, title, message, priority)
    }
}