package com.sipadu.android.server

import com.sipadu.android.model.*
import io.reactivex.Observable
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.http.*

interface ServerApi {

    @FormUrlEncoded
    @POST("login.php")
    fun login(
        @Field("username") username: String,
        @Field("password") password: String
    ): Observable<Profile>

    @Multipart
    @POST("signUp.php")
    fun signUp(
        @PartMap map: HashMap<String, RequestBody>,
        @Part file: MultipartBody.Part
    ): Observable<String>

    @Multipart
    @POST("signUp.php")
    fun signUpWithoutImage(@PartMap map: HashMap<String, RequestBody>): Observable<String>

    @FormUrlEncoded
    @POST("updateFcmToken.php")
    fun updateFcmToken(
        @Field("user_id") userId: String, @Field("fcm_token") fcmToken: String
    ): Observable<String>

    @FormUrlEncoded
    @POST("checkUser.php")
    fun checkUser(@Field("user_id") userId: String): Observable<String>

    @POST("listRegisteredUser.php")
    fun loadRegisteredUsers(): Observable<List<Profile>>

    @POST("listAllUsers.php")
    fun loadAllUsers(): Observable<List<Profile>>

    @POST("listEmployee.php")
    fun loadEmployee(): Observable<List<Profile>>

    @FormUrlEncoded
    @POST("activateUser.php")
    fun activateUser(
        @Field("user_id") userId: String, @Field("active") active: String
    ): Observable<String>

    @FormUrlEncoded
    @POST("updateProfile.php")
    fun updateProfile(@FieldMap map: HashMap<String, String>): Observable<String>

    @Multipart
    @POST("updateProfilePic.php")
    fun updateProfilePic(
        @PartMap map: HashMap<String, RequestBody>,
        @Part file: MultipartBody.Part
    ): Observable<UrlData>

    @FormUrlEncoded
    @POST("changePassword.php")
    fun changePassword(
        @Field("user_id") userId: String, @Field("password") password: String
    ): Observable<String>

    @FormUrlEncoded
    @POST("deleteUser.php")
    fun deleteUser(@Field("user_id") userId: String): Observable<String>

    @POST("listPengaduan.php")
    fun loadAllComplaints(): Observable<List<Complaint>>

    @FormUrlEncoded
    @POST("listPengaduan.php")
    fun loadComplaints(@Field("user_id") userId: String): Observable<List<Complaint>>

    @Multipart
    @POST("createPengaduan.php")
    fun createComplaintWithoutImage(@PartMap map: HashMap<String, RequestBody>): Observable<String>

    @Multipart
    @POST("createPengaduan.php")
    fun createComplaint(
        @PartMap map: HashMap<String, RequestBody>,
        @Part file: MultipartBody.Part
    ): Observable<String>

    @FormUrlEncoded
    @POST("trackLocation.php")
    fun trackLocation(
        @Field("user_id") userId: String,
        @Field("lat") lat: String,
        @Field("lng") lng: String
    ): Observable<String>

    @FormUrlEncoded
    @POST("listTrackingLocation.php")
    fun loadTrackingLocation(@Field("user_id") userId: String): Observable<List<Location>>

    @Multipart
    @POST("createKejadian.php")
    fun createIncident(@PartMap map: HashMap<String, RequestBody>): Observable<String>

    @POST("listKejadian.php")
    fun loadIncidentTypes(): Observable<List<Incident>>

    @FormUrlEncoded
    @POST("createKejadianWarga.php")
    fun createUserIncident(@FieldMap map: HashMap<String, String>): Observable<String>

    @POST("listKejadianWarga.php")
    fun loadUserIncident(): Observable<List<UserIncident>>

    @POST("listInfo.php")
    fun loadInfo(): Observable<List<Info>>

    @Multipart
    @POST("createInfo.php")
    fun createInfoWithoutImage(@PartMap map: HashMap<String, RequestBody>): Observable<String>

    @Multipart
    @POST("createInfo.php")
    fun createInfo(
        @PartMap map: HashMap<String, RequestBody>,
        @Part file: MultipartBody.Part
    ): Observable<String>

    @Multipart
    @POST("createEmployeeReport.php")
    fun createEmployeeReport(
        @PartMap map: HashMap<String, RequestBody>,
        @Part file: MultipartBody.Part
    ): Observable<String>

    @Multipart
    @POST("createEmployeeReport.php")
    fun createEmployeeReportWithoutImage(@PartMap map: HashMap<String, RequestBody>): Observable<String>

    @POST("listEmployeeReport.php")
    fun loadAllReports(): Observable<List<Report>>

    @FormUrlEncoded
    @POST("listEmployeeReport.php")
    fun loadUserReports(@Field("user_id") userId: String): Observable<List<Report>>
}