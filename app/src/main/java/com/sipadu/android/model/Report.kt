package com.sipadu.android.model

class Report {
    var id: String = "0"
    var user_id: String = "0"
    var lat: String = ""
    var lng: String = ""
    var content: String = ""
    var title: String = ""
    var photo: String = ""
    var fullname: String = ""
    var created_date: String = ""
}