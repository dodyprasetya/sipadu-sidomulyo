package com.sipadu.android.model

class Profile {
    var id: String = "0"
    var fullname: String = ""
    var email: String = ""
    var phone: String = ""
    var profile_pic: String = ""
    val fcm_token: String = ""
    val created_date: String = ""
    val type: String = ""
    var nik: String = ""
    var address: String = ""
    val position: String = ""
    val active: String = ""
    var no_kk: String = ""
    var rt: String = ""
    var rw: String = ""
}