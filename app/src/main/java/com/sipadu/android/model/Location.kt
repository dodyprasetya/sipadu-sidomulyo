package com.sipadu.android.model

class Location {
    var id: String = "0"
    var user_id: String = "0"
    var lat: String = ""
    var lng: String = ""
    var created_date: String = ""
}