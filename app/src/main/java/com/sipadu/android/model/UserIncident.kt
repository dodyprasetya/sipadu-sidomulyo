package com.sipadu.android.model

class UserIncident {
    var id: String = "0"
    var user_id: String = "0"
    var kejadian_id: String = "0"
    var name: String = ""
    var lat: String = ""
    var lng: String = ""
    var detail: String = ""
    var rt: String = ""
    var rw: String = ""
    var fullname: String = ""
    var phone: String = ""
    var created_date: String = ""
}