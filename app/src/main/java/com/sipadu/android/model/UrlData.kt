package com.sipadu.android.model

data class UrlData (
    val url: String
)