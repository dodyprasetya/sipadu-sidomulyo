package com.sipadu.android.model

class Info {
    var id: String = "0"
    var user_id: String = "0"
    var title: String = ""
    var content: String = ""
    var photo: String = ""
    var fullname: String = ""
    var created_date: String = ""
}